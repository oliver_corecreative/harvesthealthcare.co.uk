<?php
/* Development */
define('SAVEQUERIES', true);
define('WP_DEBUG', true);
define('SCRIPT_DEBUG', true);

define('FS_METHOD', 'direct');

define( 'WP_MEMORY_LIMIT', '256M' );
define( 'WP_MAX_MEMORY_LIMIT', '512M' );

define( 'WP_CACHE', false );
define( 'WPCACHEHOME', '/var/www/vhosts/harvesthealthcare.cc.com/harvesthealthcare/web/app/plugins/wp-super-cache/');
