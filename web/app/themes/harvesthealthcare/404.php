<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">

            <?php get_template_part('templates/page', 'header'); ?>

            <div class="alert alert-warning">
                <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
            </div>

            <?= \Roots\Sage\Utils\get_search_form(); ?>

        </div>
    </div>
</div>