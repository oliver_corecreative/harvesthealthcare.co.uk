<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">

            <?php get_template_part('templates/page', 'header-bar'); ?>


            <?php if (!have_posts()) : ?>
                <div class="alert alert-warning">
                    <?php _e('Sorry, no results were found.', 'sage'); ?>
                </div>
                <?php get_search_form(); ?>
            <?php endif; ?>

            <div id="posts-container">
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                <?php endwhile; ?>
            </div>

            <?php the_posts_navigation(array(
                'prev_text' => 'View more articles',
                'next_text' => 'View newer articles'
            )); ?>

        </div>
        <div class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 sidebar">

            <?php get_template_part('templates/sidebar'); ?>

        </div>
    </div>
</div>

<?php get_template_part('templates/blocks/link-blocks'); ?>