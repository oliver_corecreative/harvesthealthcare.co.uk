(function($) {

    var App = {

        Sliders: {
            Banner: function() {

                var $element = $("section.banner .wrapper");

                /*	CarouFredSel: a circular, responsive jQuery carousel.
                 Configuration created by the "Configuration Robot"
                 at caroufredsel.dev7studios.com
                 */
                $element.imagesLoaded( function() {
                    $element.carouFredSel({
                        responsive: true,
                        width: "100%",
                        items: {
                            visible: 1,
                            height: "auto",
                            start: "random"
                        },
                        auto: 6500,
                        pagination: $element.next().find('.container'),
                        swipe: true,

                        start: "random"
                    });
                });

            },
            Testimonials: function() {

                var $element = jQuery('section.testimonials .wrapper');

                $element.imagesLoaded( function() {
                    $element.carouFredSel({
                        responsive: true,
                        height: "auto",
                        items: {
                            visible: 1
                        },
                        width: "100%",
                        auto: 5000,
                        align: "center",
                        swipe: true
                    });
                });

            },
            Gallery: function() {
                var $element = $(".gallery.slider");
                $element.imagesLoaded(function() {

                    $element.carouFredSel({
                        circular: true,
                        height: "auto",
                        width: "100%",
                        items: {
                            visible: 1,
                            width: "100%",
                            height: "variable"
                        },
                        scroll: {
                            fx: "crossfade",
                            onAfter: function(data) {
                                var _visible_item = $(data.items.visible[0]);
                                var _index = _visible_item.data('index');
                                $(".thumbnail-wrapper a").removeClass('active');
                                $(".thumbnail-wrapper a").eq(_index).addClass('active');
                            }
                        },
                        align: "left",
                        auto: 6000,
                        onCreate: function(data) {

                            var $carousel = $(this);
                            $(window).resize(function() {
                                $carousel.children().each(function() {
                                    $(this).width($carousel.parent().width());
                                });
                            }).trigger('resize');

                        }
                    });

                    $(".thumbnail-wrapper a").on("click", function(e) {
                        e.preventDefault();
                        $element.trigger("slideTo", $element.find('[data-index=' + $(this).parent().index() + ']'));
                    });

                });
            },
            Categories: function() {

                var $element = $("section.categories.slider .wrapper");
                $element.imagesLoaded(function() {

                    $element.carouFredSel({
                        responsive: true,
                        height: "auto",
                        items: {
                            visible: 1,
                            filter: ".row"
                        },
                        width: "100%",
                        auto: 6000,
                        prev: {
                            button: "section.categories.slider .prev",
                            key: "left"
                        },
                        next: {
                            button: "section.categories.slider .next",
                            key: "right"
                        }
                    });

                });

            },
            Thumbnails: function() {

                var $element = $(".product-thumbnails .slider-wrapper");
                if($element.find('.row').length > 1) {
                    $element.imagesLoaded(function () {

                        $element.carouFredSel({
                            responsive: true,
                            height: "auto",
                            auto: 6000,
                            items: {
                                visible: 1,
                                filter: ".row",
                                minimum: 2
                            },
                            width: "100%",
                            prev: {
                                button: ".product-thumbnails .prev",
                                key: "left"
                            },
                            next: {
                                button: ".product-thumbnails .next",
                                key: "right"
                            }
                        });

                    });
                } else {
                    $(".product-thumbnails .prev, .product-thumbnails .next").hide();
                }

            }
        },
        Utils: {
            vertical_center: function(elements) {

                $(elements).each(function() {

                    var _height = $(this).height();
                    var _item_height = $(this).children().eq(0).outerHeight();
                    var _item_add_on = (_height-_item_height)/2;
                    $(this).children().css("marginTop", Math.ceil(_item_add_on));

                });

            },
            svg_fallback: function() {

                if (!Modernizr.svg) {
                    var imgs = document.getElementsByTagName('img');
                    var svgExtension = /.*\.svg$/;
                    var l = imgs.length;
                    for(var i = 0; i < l; i++) {
                        if(imgs[i].src.match(svgExtension)) {
                            imgs[i].src = imgs[i].src.slice(0, -3) + 'png';
                        }
                    }
                }

            },
            equal_height: {
                init: function(container) {
                    $(container).imagesLoaded(function() {

                        App.Utils.equal_height.run(container);

                        $(window).resize(function(){
                            App.Utils.equal_height.run(container);
                        });
                        $( '.woocommerce-tabs ul.tabs li a' ).click( function() {
                            setTimeout(function(){
                                App.Utils.equal_height.run(container);
                            }, 200);
                        });

                    });
                },
                run: function(container) {

                    var currentTallest = 0,
                        currentRowStart = 0,
                        rowDivs = [],
                        $el,
                        topPosition = 0;
                    $(container).each(function() {

                        $el = $(this);
                        $($el).height('auto');
                        topPostion = $el.position().top;

                        if (currentRowStart !== topPostion) {
                            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                                rowDivs[currentDiv].height(currentTallest);
                            }
                            rowDivs.length = 0; // empty the array
                            currentRowStart = topPostion;
                            currentTallest = $el.height();
                            rowDivs.push($el);
                        } else {
                            rowDivs.push($el);
                            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                        }
                        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                            rowDivs[currentDiv].height(currentTallest);
                        }
                    });

                }
            }
        },
        Map: {

            geocoder: null,
            instance: null,

            init: function() {

                if($("#map-container").length > 0) {
                    App.Map.geocoder = new google.maps.Geocoder();

                    google.maps.event.addDomListener(window, "load", App.Map.run);

                }

            },
            run: function() {

                App.Map.geocoder.geocode({
                    address: $("#address").text()
                }, function(results, status) {
                    App.Map.instance = new google.maps.Map(document.getElementById("map-container"), {
                        center: results[0].geometry.location,
                        zoom: 10,
                        disableDefaultUI: true,
                        scrollwheel: false,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.TOP_RIGHT,
                            style: google.maps.ZoomControlStyle.SMALL
                        },
                        styles: [
                            {
                                "featureType": "water",
                                "stylers": [
                                    { "visibility": "on" },
                                    { "saturation": -60 }
                                ]
                            },{
                                "featureType": "road.highway",
                                "stylers": [
                                    { "saturation": -100 }
                                ]
                            },{
                                "featureType": "road.highway",
                                "elementType": "labels",
                                "stylers": [
                                    { "visibility": "off" }
                                ]
                            }
                        ]
                    });

                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: App.Map.instance,
                        title: 'Northgate Head Office'
                    });
                    App.Map.resize(results[0].geometry.location);
                });

            },
            resize: function(location) {

                $(window).smartresize(function() {
                    App.Map.instance.setCenter(location);
                });

            }
        },
        News: {
            init: function() {
                App.News.pagination();
            },
            pagination: function() {

                var container = "#posts-container";
                var button = ".nav-links .nav-previous a";

                $(document).on('click', button, function(e) {
                    e.preventDefault();

                    var $this = $(this);
                    $this.addClass("disabled");

                    $.get( $this.attr('href'), function( data ) {

                        $(container).append($(container + ' article', data));

                        if($(button, data).length < 1) {
                            $this.remove();
                        } else {
                            $this.removeClass("disabled");
                            $this.attr("href", $(button, data).attr("href"));
                        }

                    });

                });

            }
        },
        Forms: {

            init: function() {

                App.Forms.FancySelect.init();
                App.Forms.FancyCheckbox.init();

                $(document).on("click", "#share_modal .form_minus", function() {

                    var $modal = $("#share_modal");
                    if($modal.find('.friend').length < 2) {

                        alert('You can\'t remove any more friends.');
                        $modal.find('.friend').eq(0).find('input').focus();

                    } else {
                        var $last_row = $modal.find('.friend').last();
                        $last_row.remove();
                    }

                });

                $(document).on("click", "#share_modal .form_plus", function() {

                    var _limit = 10;
                    var $modal = $("#share_modal");
                    if($modal.find('.friend').length >= _limit) {

                        alert('You can add a maximum of 10 friends.');

                    } else {

                        var $new_row = $modal.find('.friend').eq(0).clone();
                        $new_row.find('input').val("");
                        $new_row.find('p.error').remove();
                        $modal.find('.friend').last().after($new_row);
                        $new_row.find('input').focus();

                    }

                });

            },
            FancySelect: {
                init: function($container) {

                    if(typeof $container === 'undefined') {
                        $container = $(document);
                    }

                    var $items = $("select.form-control.fancy", $container);
                    $items.wrap("<span class='cc-select' />");
                    $items.on('change', App.Forms.FancySelect.change).trigger('change');

                },
                change: function() {
                    var $wrapper = $(this).parent("span.cc-select");
                    if(!$(this).val()) {
                        $wrapper.addClass('placeholder');
                    } else {
                        $wrapper.removeClass('placeholder');
                    }
                }
            },
            FancyCheckbox: {
                init: function($container) {

                    if(typeof $container === 'undefined') {
                        $container = $(document);
                    }

                    var $items = $("input[type=checkbox].fancy", $container);
                    $items.each(function() {

                        var _class = '';
                        if($(this).hasClass('form-control-large')) {
                            _class = 'cc-checkbox-large';
                        }
                        $(this).wrap("<span class='cc-checkbox " + _class + "' />");


                    });
                    $items.on('change', App.Forms.FancyCheckbox.change).trigger('change');

                },
                change: function() {
                    if($(this).is(":checked")) {
                        $(this).parents(".form-group").addClass("checked");
                        $(this).parents(".cc-checkbox").addClass("checked");
                    } else {
                        $(this).parents(".form-group").removeClass("checked");
                        $(this).parents(".cc-checkbox").removeClass("checked");
                    }
                }
            },

        },
        Lightbox: {
            init: function() {
                jQuery("a[rel^='prettyPhoto']").prettyPhoto({
                    social_tools: false,
                    opacity: 0.8,
                    deeplinking: false,
                    default_width: 748,
                    default_height: 422,
                });
            }
        }

    };
    window.App = App;

})(jQuery);