/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {
                // JavaScript to be fired on all pages
                App.Sliders.Banner();
                App.Sliders.Testimonials();
                App.Sliders.Gallery();
                App.Sliders.Categories();

                App.Utils.svg_fallback();
                App.Utils.equal_height.init('article.type-product');

                App.Map.init();
                App.Forms.init();
                App.Lightbox.init();



                var $input = $("input.qty");
                $input.qtyStep({
                    buttonClass: "btn btn-primary"
                });

                var change_event = debounce(function() {
                    $(this).parents("form").submit();
                }, 1000);
                $input.on("change", change_event);


            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function () {
                // JavaScript to be fired on the home page
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'archive': {
            init: function () {

                App.News.init();

            }
        },
        'maintenance_repair': {
            init: function () {

                App.Utils.equal_height.init('.blocks .row > div');

            }
        },
        'single_product': {
            init: function () {

                App.Sliders.Thumbnails();
                //App.Utils.equal_height.init('.woocommerce-tabs .panel ul li');
                App.Utils.equal_height.init('.woocommerce-tabs .panel .media-wrapper');

                $(".woocommerce-main-image.product-gallery").on('click', function(e) {
                    e.preventDefault();
                    $(".product-thumbnails .slider-wrapper a").eq(0).trigger('click');
                });

            }
        },
        'downloads': {
            init: function() {

                jQuery(window).load(function() {
                    App.Utils.equal_height.init('.download-sections .form-group');
                });

            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
