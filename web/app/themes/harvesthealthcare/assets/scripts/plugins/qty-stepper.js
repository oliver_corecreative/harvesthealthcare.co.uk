/**
 * Created by iMac9 on 23/06/2016.
 */
(function ( $ ) {

    $.fn.qtyStep = function( options ) {

        var settings = $.extend({
            buttonClass: "btn",
            element: "button",
            callback: function() {}
        }, options );

        var buttonClick = function(e, input, direction) {
            e.preventDefault();

            var step = parseInt(input.attr('step') || 1);
            var min = parseInt(input.attr('min') || 1);
            var max = parseInt(input.attr('max') || 999999);
            var val = parseInt(input.val());

            if(direction === "down") {
                val += -(step);
            } else {
                val += (step);
            }

            if(val <= min) {
                val = min;
                $(this).prop('disabled', true);
            } else if (val >= max) {
                val = max;
            }

            input.val(val);
            input.trigger('change');

        };

        var inputChange = function($this) {

            var val = parseInt($this.val());
            var min = parseInt($this.attr('min') || 1);
            var max = parseInt($this.attr('max') || 999999);

            if(val === min) {
                $this.prev().prop("disabled", true);
            } else {
                $this.prev().prop("disabled", false);
            }

            if(val === max) {
                $this.next().prop("disabled", true);
            } else {
                $this.next().prop("disabled", false);
            }

        };

        this.each(function() {
            var $this = $(this);
            var $button = $("<" + settings.element + "/>").addClass(settings.buttonClass);

            $button.on("click", function(e, step) {
                var direction = "up";
                if($(this).hasClass("qty-minus")) {
                    direction = "down";
                }
                buttonClick(e, $this, direction);
            });

            $this.before($button.clone(true).addClass("qty-minus").text("-"));
            $this.after($button.clone(true).addClass("qty-plus").text("+"));

            settings.callback();

            $this.on("change.qty", function() {
                inputChange($(this));
            }).trigger("change.qty");


        });


    };

}( jQuery ));