<?php get_template_part('templates/blocks/link-blocks'); ?>

<?php while (have_posts()) : the_post(); ?>
<div class="container entry entry-main entry-<?= $post->post_name; ?>">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h1 class="entry-title"><?php the_title(); ?></h1>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</div>
<?php endwhile; ?>

<?php get_template_part("templates/blocks/category-slider"); ?>

<?php get_template_part("templates/blocks/usps"); ?>