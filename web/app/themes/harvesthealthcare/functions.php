<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/functions.php',
    'lib/utils.php',                 // Utility functions
    'lib/init.php',                  // Initial theme setup and constants
    'lib/wrapper.php',               // Theme wrapper class
    'lib/conditional-tag-check.php', // ConditionalTagCheck class
    'lib/config.php',                // Configuration
    'lib/assets.php',                // Scripts and stylesheets
    'lib/titles.php',                // Page titles
    'lib/extras.php',                // Custom functions
    'lib/comments.php',
    'lib/custom.php',
    'lib/option.php',
    'lib/post-types.php',
    'lib/forms.php',
    'lib/gallery.php',
    'woocommerce/overrides.php',
    'woocommerce/config.php',
    'lib/nav-walker.php',
    'lib/banner.php',
    'lib/ajax.php',
    'lib/Zip.php'
];

foreach ($sage_includes as $file) {
    if (!$filepath = locate_template($file)) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
    }

    require_once $filepath;
}
unset($file, $filepath);


/*--------------------------------------------------------------------------------------------------
 Don't show Catagories -
--------------------------------------------------------------------------------------------------*/
add_filter( 'get_terms', 'get_subcategory_terms', 10, 3 );
function get_subcategory_terms( $terms, $taxonomies, $args ) {
    $new_terms = array();
    // if a product category and on the shop page
    if ( in_array( 'product_cat', $taxonomies ) && ! is_admin() ) {
        foreach ( $terms as $key => $term ) {
            if ( ! in_array( $term->slug, array( 'clearance' ) ) ) {
                $new_terms[] = $term;
            }
        }
        $terms = $new_terms;
    }
    return $terms;
}