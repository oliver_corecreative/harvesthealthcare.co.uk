<?php

new CC_AJAX('downloads_other', function() {

    if(empty($_POST['files'])) die('Please select one or more files...');

    $files = array();
    foreach($_POST['files'] as $file_id) {
        $files[] = get_attached_file($file_id);
    }

    $zip = new Zip();
    foreach($files as $file) {
        $zip->addFile( file_get_contents($file), basename($file), filectime( $file ) );
    }

    $zip->sendZip("harvest-downloads-" . date('d-m-y') . '.zip');

    exit;
});
new CC_AJAX('product_import', function() {
    require dirname(__FILE__).'/import.php';
    exit;
});