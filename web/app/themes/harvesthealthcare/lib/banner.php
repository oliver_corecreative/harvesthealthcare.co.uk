<?php

namespace Roots\Sage\Banner;

add_filter('banner_slides', function($slides) {

    if(!empty($slides)) return $slides;

    if(is_product_category()) {

        $category = get_queried_object();

        if($category->parent) {
            $category_parts = array($category->name);
            $category = get_term_by('id', $category->parent, $category->taxonomy);
            array_unshift($category_parts, $category->name . ":");
        } else {
            $category_parts = \Roots\Sage\Extras\array_divide(explode(' ', $category->name), 3);
            foreach($category_parts as &$row) {
                $row = implode(' ', $row);
            }
        }

        $image_path = \Roots\Sage\Assets\asset_path('images/category-banners/' . $category->slug . '.png', false);

        if(!file_exists($image_path)) {
            return array(
                new Banner_Slide(array(
                    'heading_text' => implode('<br />', $category_parts)
                ))
            );
        }

        list($w, $h) = (getimagesize($image_path));

        return array(
            new Banner_Slide(array(
                'heading_text' => implode('<br />', $category_parts),
                'image' => array(
                    'url' => \Roots\Sage\Assets\asset_path('images/category-banners/' . $category->slug . '.png'),
                    'width' => $w,
                    'height' => $h
                ),
                'image_position_vertical' => 'bottom',
            ))
        );

    }

    if(is_product()) {

        return false;

        $product = get_queried_object();
        $categories = get_the_terms( $product->id, 'product_cat' );
        $category = $categories[0];

        $category_parts = \Roots\Sage\Extras\array_divide(explode(' ', $category->name), 3);
        foreach($category_parts as &$row) {
            $row = implode(' ', $row);
        }

        $image_path = \Roots\Sage\Assets\asset_path('images/category-banners/' . $category->slug . '.png', false);

        list($w, $h) = (getimagesize($image_path));

        return array(
            new Banner_Slide(array(
                'heading_text' => implode('<br />', $category_parts),
                'image' => array(
                    'url' => \Roots\Sage\Assets\asset_path('images/category-banners/' . $category->slug . '.png'),
                    'width' => $w,
                    'height' => $h
                ),
                'image_position_vertical' => 'bottom',
            ))
        );

    }

    if(is_post_type_archive('post') || is_singular('post')) {

        return array(
            new Banner_Slide(array(
                'heading_text' => 'Latest <br />news from <br />Harvest',
                'image' => new Banner_Slide_Image('News Banner'),
                'image_position_vertical' => 'bottom',
                'image_position_horizontal_offset' => 200
            ))
        );

    }

    if(is_post_type_archive('case_study') || is_singular('case_study')) {

        return array(
            new Banner_Slide(array(
                'heading_text' => 'Project <br />Case <br />Studies',
                'image' => new Banner_Slide_Image('Case Study Banner'),
                'image_position_vertical' => 'bottom',
                'image_position_horizontal_offset' => 200
            ))
        );

    }

});

add_filter('banner_height', function($height) {

    if(empty($height)) $height = 294;

    return $height;

});

class Banner_Slide implements \ArrayAccess {

    protected $data = array(
        'heading_text' => '',
        'link_text' => '',
        'link_url' => '',
        'show_button' => true,
        'button_text' => '',
        'button_url' => '',
        'image_position_vertical' => 'center',
        'image_position_vertical_offset' => 0,
        'image_position_horizontal_offset' => 0
    );

    public function __construct($data = array()) {

        $this->data = array_merge($this->data, $data);

    }



    public function offsetSet($offset, $value) {
        $this->data[$offset] = $value;
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

}

class Banner_Slide_Image implements \ArrayAccess {

    private $data = array(
        'width' => 0,
        'height' => 0,
        'url' => ''
    );

    public function __construct($id) {

        if(!is_int($id)) {
            $image = wp_get_attachment_by_post_name($id);
        }

        $attachment = wp_get_attachment_image_src($image->ID, 'full');

        if(!$attachment)
            trigger_error('No image found: (' . $id . ')', E_USER_WARNING);

        $image = array(
            'url' => $attachment[0],
            'width' => $attachment[1],
            'height' => $attachment[2]
        );

        $this->data = $image;

    }



    public function offsetSet($offset, $value) {
        $this->data[$offset] = $value;
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

}