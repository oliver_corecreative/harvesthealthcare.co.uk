<?php

namespace Roots\Sage\Config;

use Roots\Sage\ConditionalTagCheck;

/**
 * Enable theme features
 */
add_theme_support('soil-clean-up');         // Enable clean up from Soil
add_theme_support('soil-nav-walker');       // Enable cleaner nav walker from Soil
add_theme_support('soil-relative-urls');    // Enable relative URLs from Soil
add_theme_support('soil-nice-search');      // Enable nice search from Soil
add_theme_support('soil-jquery-cdn');
add_theme_support('soil-js-to-footer');
add_theme_support('soil-disable-trackbacks');
add_theme_support('bootstrap-gallery');
add_theme_support('woocommerce');

/**
 * Image Sizes
 */
add_image_size('product-category', 500, 320, true);
add_image_size('gallery', 600, 383, true);
add_image_size('gallery-small', 200, 150, true);

/**
 * Configuration values
 */
if (!defined('WP_ENV')) {
    // Fallback if WP_ENV isn't defined in your WordPress config
    // Used in lib/assets.php to check for 'development' or 'production'
    define('WP_ENV', 'production');
}

if (!defined('DIST_DIR')) {
    // Path to the build directory for front-end assets
    define('DIST_DIR', '/dist/');
}

if (!defined('MAPS_API_KEY')) {
    define('MAPS_API_KEY', 'AIzaSyAc4gF4vE1I6eADntZGn6vkPPFdvK1sufs');
}

/**
 * Define which pages shouldn't have the sidebar
 */
function display_sidebar()
{
    static $display;

    if (!isset($display)) {
        $conditionalCheck = new ConditionalTagCheck(
        /**
         * Any of these conditional tags that return true won't show the sidebar.
         * You can also specify your own custom function as long as it returns a boolean.
         *
         * To use a function that accepts arguments, use an array instead of just the function name as a string.
         *
         * Examples:
         *
         * 'is_single'
         * 'is_archive'
         * ['is_page', 'about-me']
         * ['is_tax', ['flavor', 'mild']]
         * ['is_page_template', 'about.php']
         * ['is_post_type_archive', ['foo', 'bar', 'baz']]
         *
         */
            [
                '__return_true',
                'is_404',
                'is_front_page',
                ['is_page_template', 'template-custom.php']
            ]
        );

        $display = apply_filters('sage/display_sidebar', $conditionalCheck->result);
    }

    return $display;
}
