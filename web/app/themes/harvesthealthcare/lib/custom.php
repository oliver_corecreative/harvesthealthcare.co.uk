<?php
/**
 * @package aureo
 * @copyright Core Creative 2015
 * @author Oliver Sadler (www.oli.digital)
 * @author Core Creative (www.corecreative.co.uk)
 */


namespace Roots\Sage;

use Roots\Sage\Option;

class App {

	public static function image($path, $width = '', $height = '', $zc = 2, $args = array()) {
		/*if(is_url($path)) {
			$path = realpath('.' . \Roots\Soil\Utils\root_relative_url($path));
		}*/
		$args['src'] = $path;
		$args['w'] = $width;
		$args['h'] = $height;
		$args['zc'] = $zc;
		return \Roots\Sage\Assets\asset_path('images/img.php') . '?' . http_build_query($args);
	}

	public static $category_defaults = array(
	);

	public static $term_cache = array();

	public static function get_main_categories($args = array()) {

		$defaults = wp_parse_args( self::$category_defaults, array(
			'taxonomy' => 'product_cat',
			'parent' => 0,
			'hierarchical' => 1,
			'hide_empty' => 0,
			'children' => false
		));
		$args = wp_parse_args( $args, $defaults );

		if(isset(self::$term_cache[md5(serialize($args))])) return self::$term_cache[md5(serialize($args))];

		$categories = get_categories( $args );

		foreach($categories as &$category) {

			if($args['taxonomy'] == 'product_cat') {

				$thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);
				$thumbnail = wp_get_attachment_image_src($thumbnail_id, 'product-category');
				$category->image = array(
					'src' => $thumbnail[0],
					'width' => $thumbnail[1],
					'height' => $thumbnail[2]
				);
			} else {
				$image = get_field('image', $category);
				$category->image = array(
					'src' => $image['sizes']['product-category'],
					'width' => $image['sizes']['product-category-width'],
					'height' => $image['sizes']['product-category-height']
				);
			}

			$category->permalink = get_term_link($category, $args['taxonomy']);

			$category->children = array();
			if($args['children']) {
				$child_args = wp_parse_args( array(
					'parent' => $category->term_id,
					'children' => false # @todo may have to change this
				), $args );
				$category->children = self::get_main_categories($child_args);
			}
		}

		self::$term_cache[md5(serialize($args))] = $categories;
		return self::$term_cache[md5(serialize($args))];

	}

	public static function get_posts($args = array()) {

		$defaults = array(
			'posts_per_page'   => -1,
			'orderby'          => 'post_date',
			'order'            => 'DESC',
			'post_type'        => 'testimonial',
			'post_status'      => 'publish'
		);
		$args = wp_parse_args( $args, $defaults );
		$posts = get_posts( $args );

		return $posts;

	}

}