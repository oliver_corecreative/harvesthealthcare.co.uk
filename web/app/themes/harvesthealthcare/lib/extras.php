<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes)
{
    // Add page slug if it doesn't exist
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    // Add class if sidebar is active
    if (Config\display_sidebar()) {
        $classes[] = 'sidebar-primary';
    }

    return $classes;
}

add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more()
{
    return ' &hellip; <a href="' . get_permalink() . '">' . __('Read More', 'sage') . '</a>';
}

add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

function attachment_lightbox($content) {
    return str_replace('a href', 'a data-rel="prettyPhoto[gallery]" href', $content);
}
add_filter('wp_get_attachment_link', __NAMESPACE__ . '\\attachment_lightbox');


function array_divide($array, $segmentCount) {
    $dataCount = count($array);
    if ($dataCount == 0) return false;
    $segmentLimit = ceil($dataCount / $segmentCount);
    $outputArray = array_chunk($array, $segmentLimit);

    return $outputArray;
}
function slug_format($str){
    $str = strtolower($str);
    $str = preg_replace('/[^a-z0-9_\s-]/', '', $str);
    $str = preg_replace("/[\s-]+/", " ", $str);
    $str = preg_replace("/[\s_]/", "-", $str);
    return (string) $str;
}


// remove the html filtering
remove_filter( 'pre_term_description', 'wp_filter_kses' );
remove_filter( 'term_description', 'wp_kses_data' );


function favicons() {
    ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-57x57.png'); ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-60x60.png'); ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-72x72.png'); ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-76x76.png'); ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-114x114.png'); ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-120x120.png'); ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-144x144.png'); ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-152x152.png'); ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= \Roots\Sage\Assets\asset_path('images/icons/apple-touch-icon-180x180.png'); ?>">
    <link rel="icon" type="image/png" href="<?= \Roots\Sage\Assets\asset_path('images/icons/favicon-32x32.png'); ?>" sizes="32x32">
    <link rel="icon" type="image/png" href="<?= \Roots\Sage\Assets\asset_path('images/icons/favicon-194x194.png'); ?>" sizes="194x194">
    <link rel="icon" type="image/png" href="<?= \Roots\Sage\Assets\asset_path('images/icons/favicon-96x96.png'); ?>" sizes="96x96">
    <link rel="icon" type="image/png" href="<?= \Roots\Sage\Assets\asset_path('images/icons/android-chrome-192x192.png'); ?>" sizes="192x192">
    <link rel="icon" type="image/png" href="<?= \Roots\Sage\Assets\asset_path('images/icons/favicon-16x16.png'); ?>" sizes="16x16">
    <link rel="manifest" href="<?= \Roots\Sage\Assets\asset_path('images/icons/manifest.json'); ?>">
    <link rel="shortcut icon" href="<?= \Roots\Sage\Assets\asset_path('images/icons/favicon.ico'); ?>">
    <meta name="apple-mobile-web-app-title" content="Harvest Healthcare"/>
    <meta name="application-name" content="Harvest Healthcare"/>
    <meta name="msapplication-TileColor" content="#9f00a7"/>
    <meta name="msapplication-TileImage" content="<?= \Roots\Sage\Assets\asset_path('images/icons/mstile-144x144.png'); ?>">
    <meta name="msapplication-config" content="<?= \Roots\Sage\Assets\asset_path('images/icons/browserconfig.xml'); ?>">
    <meta name="theme-color" content="#ac208d"/>
    <?php
}

add_action( 'wp_head', __NAMESPACE__ . '\\favicons', 15);

function lead_forensics() {
    ?>
    <script type="text/javascript" src="https://secure.leadforensics.com/js/83913.js" ></script>
    <noscript><img alt="" src="https://secure.leadforensics.com/83913.png" style="display:none;" /></noscript>
    <?php
}
add_action( 'wp_footer', __NAMESPACE__ . '\\lead_forensics', 100);



new \CC_Shortcode('date');
new \CC_Shortcode('option');
new \CC_Shortcode('option_wp');
new \CC_Shortcode('protect_email');
