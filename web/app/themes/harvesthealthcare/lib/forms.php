<?php

CC_Form::get_instance()->add_form('contact', array(
    'name' => array(
        'label' => 'Name',
        'required' => true
    ),
    'company' => array(
        'label' => 'Company/NHS Trust',
        'required' => true
    ),
    'telephone' => 'Telephone',
    'job-title' => 'Job Title',
    'address-line-1' => array(
        'label' => 'Address Line 1'
    ),
    'address-line-2' => array(
        'label' => 'Address Line 2'
    ),
    'town-city' => array(
        'label' => 'Town/City'
    ),
    'postcode' => array(
        'label' => 'Postcode'
    ),
    'email' => array(
        'label' => 'Email',
        'required' => true,
        'rules' => 'email'
    ),
    'subject' => 'Subject',
    'message' => array(
        'label' => 'How can we help you?',
        'required' => true
    ),
    'call-back' => array(
        'label' => 'Request a call back',
        'required' => false
    ),
), array(
    'send_email' => 'sales@harvesthealthcare.co.uk',
    'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
    'success_message' => 'Thank you for your enquiry. A member of our team will contact you shortly.'
));

CC_Form::get_instance()->add_form('maintenance', array(
    'name' => array(
        'label' => 'Name',
        'required' => true
    ),
    'company' => array(
        'label' => 'Company'
    ),
    'telephone' => 'Telephone',
    'email' => array(
        'label' => 'Email',
        'required' => true,
        'rules' => 'email'
    ),
    'message' => array(
        'label' => 'Your enquiry...',
        'required' => true
    )
), array(
    'send_email' => 'servicing@harvesthealthcare.co.uk',
    'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
    'success_message' => 'Thank you for your enquiry. A member of our team will contact you shortly.'
));

CC_Form::get_instance()->add_form('share', array(
    'name' => array(
        'label' => 'Name',
        'required' => true
    ),
    'email' => array(
        'label' => 'Email',
        'required' => true,
        'rules' => 'email'
    ),
    'friends_email' => array(
        'label' => 'Friend\'s Emails',
        'required' => true
    ),
    'message' => array(
        'label' => 'Message',
        'required' => true
    ),
    'url' => array(
        'label' => 'URL',
        'required' => true
    ),
    'copy_to_own_email' => array(
        'label' => 'Copy to my email address'
    )
), array(
    'send_email' => false,
    'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
    'success_message' => 'Your messages have been sent successfully.',
    'add_post' => false,
    'callback' => function($post_fields) {

        $subject = 'A product from Harvest Healthcare has been shared with you...';
        $from = $post_fields['name']['value'] . ' ('.$post_fields['email']['value'].') via ' . $_SERVER['HTTP_HOST'] . ' <no-reply@'.$_SERVER['HTTP_HOST'].'>';

        $email_body = $post_fields['message']['value'] . "\r\n\r\n\r\n";
        $email_body .= "--------------------------------------------------------------\r\n\r\n";
        $email_body .= "View the page shared with you: " . $_REQUEST['url'];

        if($post_fields['copy_to_own_email']['value'] == 1)
            $post_fields['friends_email']['value'][] = $post_fields['email']['value'];

        foreach($post_fields['friends_email']['value'] as $friend_email) {

            $send_email = wp_mail($friend_email, $subject, $email_body, array(
                "From: " . $from,
                "Reply-to: " . $post_fields['email']['value'],
                "Content-type: text/plain"
            ));
            if(!$send_email)
                return FALSE;

        }
        return TRUE;

    }
));

CC_Form::get_instance()->add_form('download-catalogue', array(
    'name' => array(
        'label' => 'Name',
        'required' => true
    ),
    'company' => array(
        'label' => 'Company/NHS Trust',
        'required' => true
    ),
    'telephone' => array(
        'label' => 'Telephone',
        'required' => true
    ),
    'job-title' => array(
        'label' => 'Job Title',
        'required' => true
    ),
    'address-line-1' => array(
        'label' => 'Address Line 1',
        'required' => true
    ),
    'address-line-2' => array(
        'label' => 'Address Line 2'
    ),
    'town-city' => array(
        'label' => 'Town/City',
        'required' => true
    ),
    'postcode' => array(
        'label' => 'Postcode',
        'required' => true
    ),
    'email' => array(
        'label' => 'Email',
        'required' => true,
        'rules' => 'email'
    ),
    'file' => array(
        'label' => 'Catalogue',
        'required' => true
    )
), array(
    'send_email' => 'sales@harvesthealthcare.co.uk',
    'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
    'success_message' => 'Thank you for your request. Your catalogue will be with you shortly.',
    'redirect' => function( $post_data ) {

        $attachment = wp_get_attachment_by_post_name($post_data['file']['value']);
        return $attachment ? force_download(wp_get_attachment_url($attachment->ID)) : "";

    }
));

CC_Form::get_instance()->add_form('request-quote', array(
    'name' => array(
        'label' => 'Name',
        'required' => true
    ),
    'company' => array(
        'label' => 'Company',
        'required' => true
    ),
    'email' => array(
        'label' => 'Email',
        'required' => true,
        'rules' => 'email'
    ),
    'telephone' => 'Telephone',
    'product' => array(
        'label' => 'Product',
        'required' => true
    ),
    'quantity' => array(
        'label' => 'Quantity',
        'required' => true
    ),
    'message' => array(
        'label' => 'Your enquiry...'
    )
), array(
    'send_email' => 'enquiries@harvesthealthcare.co.uk',
    'error_message' => 'There were errors with your submission. Please correct the errors below and try again.',
    'success_message' => 'Thank you for your request. A member of our team will contact you shortly.'
));