<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 16/07/15
 * Time: 15:39
 */
function get_image_sizes( $size = '' ) {

    global $_wp_additional_image_sizes;

    $sizes = array();
    $get_intermediate_image_sizes = get_intermediate_image_sizes();

    // Create the full array with sizes and crop info
    foreach( $get_intermediate_image_sizes as $_size ) {

        if ( in_array( $_size, array( 'thumbnail', 'medium', 'large' ) ) ) {

            $sizes[ $_size ]['width'] = get_option( $_size . '_size_w' );
            $sizes[ $_size ]['height'] = get_option( $_size . '_size_h' );
            $sizes[ $_size ]['crop'] = (bool) get_option( $_size . '_crop' );

        } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {

            $sizes[ $_size ] = array(
                'width' => $_wp_additional_image_sizes[ $_size ]['width'],
                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                'crop' =>  $_wp_additional_image_sizes[ $_size ]['crop']
            );

        }

    }

    // Get only 1 size if found
    if ( $size ) {

        if( isset( $sizes[ $size ] ) ) {
            return $sizes[ $size ];
        } else {
            return false;
        }

    }

    return $sizes;
}

if(!function_exists('wp_get_attachment_by_post_name')) {
    function wp_get_attachment_by_post_name( $post_name ) {
        global $wpdb;

        $post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE post_title = %s AND post_type='attachment'", $post_name ));

        if ( $post )
            return $post;
        else
            return false;

    }
}




function is_url( $url ) {
    return filter_var( $url, FILTER_VALIDATE_URL );
}
function nice_title($slug) {
    return ucwords(trim(str_replace(array("-", "_"), array(" ", " "), $slug)));
}
function slugFormat($str){
    $str = strtolower($str);
    $str = preg_replace('/[^a-z0-9_\s-]/', '', $str);
    $str = preg_replace("/[\s-]+/", " ", $str);
    $str = preg_replace("/[\s_]/", "-", $str);
    return (string) $str;
}
function arrayToSelect( $array, $selected = ''){

    $output = "";

    foreach($array as $value=>$text){

        if(is_int($value))
            $value = $text;

        if(is_array($text)){

            $output .= '<optgroup id="'.$value.'" label="'.$text['label'].'">';
            unset($text['label']);
            $output .= arrayToSelect($text, $selected); //YAY RECURSIVE!!
            $output .= '</optgroup>' . "\n";

        }else{

            $output .= '<option title="'.$text.'" value="'.$value.'" ';
            if($value == $selected){

                $output .= 'selected=selected';

            }
            $output .=	' >'.$text.'</option>' . "\n";

        }

    }
    return ($output);

}

function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
    $numbers = range($min, $max);
    shuffle($numbers);
    return array_slice($numbers, 0, $quantity);
}