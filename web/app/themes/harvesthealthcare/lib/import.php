<?php

set_time_limit(0);
ini_set('auto_detect_line_endings',TRUE);
header("Content-type: text/plain");


$file_path = '/var/www/public/harvest/import.csv';

/**
 * Convert a comma separated file into an associated array.
 * The first row should contain the array keys.
 *
 * Example:
 *
 * @param string $filename Path to the CSV file
 * @param string $delimiter The separator used in the file
 * @return array
 * @link http://gist.github.com/385876
 * @author Jay Williams <http://myd3.com/>
 * @copyright Copyright (c) 2010, Jay Williams
 * @license http://www.opensource.org/licenses/mit-license.php MIT License
 */
function csv_to_array($filename='', $delimiter=',')
{
    if(!file_exists($filename) || !is_readable($filename))
        return FALSE;

    $header = NULL;
    $data = array();
    if (($handle = fopen($filename, 'r')) !== FALSE)
    {
        while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
        {
            if(!$header)
                $header = $row;
            else
                $data[] = array_combine($header, $row);
        }
        fclose($handle);
    }
    return $data;
}


$rows = csv_to_array($file_path);

foreach($rows as &$product) {

    cc_create_post($product);
    cc_images($product);
    cc_additional_features($product);

}
foreach($rows as &$product) {

    cc_accessories($product);

}


function cc_images($product) {

    $product_gallery = '';
    $images = explode(',', $product['Image Filename']);
    $i = 0;
    foreach($images as $image) {
        $image = trim($image);

        if(empty($image)) continue;
        $thumbnail_id = upload_file('/var/www/public/harvest/products/' . $image);


        if($i == 0) {
            set_post_thumbnail( $product['id'], $thumbnail_id );
        }

        $product_gallery .= $thumbnail_id . ',';

        $i++;
    }

    update_post_meta($product['id'], '_product_image_gallery', trim($product_gallery, ','));

}



function cc_create_post(&$product) {

    $new_post = array(
        'post_title'    => $product['Title'],
        'post_content'  => $product['Description'],
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type' => 'product',
        'post_parent' => ''
    );

    // Insert the post into the database
    $post_id = wp_insert_post( $new_post );
    $product['id'] = $post_id;

    // set category
    wp_set_object_terms( $post_id, $product['Category'], 'product_cat' );

    // set sku
    update_post_meta( $post_id, '_sku', $product['Code']);

    // visibility
    if($product['Catalogue Visible'] === '0')
        update_post_meta( $post_id, '_visibility', 'hidden');
    else
        update_post_meta( $post_id, '_visibility', 'visible');


    // set other defaults
    update_post_meta( $post_id, '_stock_status', 'instock');
    update_post_meta( $post_id, '_manage_stock', "no" );
    wp_set_object_terms($post_id, 'simple', 'product_type');


    // custom fields
    update_post_meta( $post_id, '_prr', 'field_55ba4914e18fa' );
    update_post_meta( $post_id, 'prr', $product['PRR'] );

    update_post_meta( $post_id, '_features', 'field_55a7b79bb2a41' );
    update_post_meta( $post_id, 'features', $product['Features'] );

    update_post_meta( $post_id, '_specifications', 'field_55a7b7aab2a42' );
    update_post_meta( $post_id, 'specifications', $product['Specifications'] );

}

function cc_additional_features($product) {
    $features_path = '/var/www/public/harvest/additional/';

    $features = explode(',', $product['Additional Features']);

    $features_to_upload = array();
    foreach($features as $item) {
        $item = trim($item);

        if(empty($item)) continue;

        list($image, $text) = explode(':', $item);

        $images_list = glob($features_path . $product['Code'] . '-' . $image . '.*');
        if(!$images_list[0]) {
            $images_list = glob($features_path . $image . '.*');

            if(!$images_list[0]) {
                echo 'No image found: ' . $image . "(".$product['Code'].")\n";
            }
        }
        $image = $images_list[0];

        $icon_id = upload_file($image);
        $features_to_upload[] = array(
            'icon' => $icon_id,
            'text' => $text
        );

        // checked

    }

    update_field('field_55a7b6cc3e12a', $features_to_upload, $product['id']);

}

function cc_accessories($product) {

    $accessory_ids = array();
    foreach(explode(',', $product['Accessories']) as $product_sku) {
        $product_sku = trim($product_sku);
        if(empty($product_sku)) continue;

        $accessory_id = get_product_by_sku($product_sku);
        if(!$accessory_id) continue;

        $accessory_ids[] = $accessory_id;

    }

    if(!empty($accessory_ids))
        update_post_meta($product['id'], '_upsell_ids', $accessory_ids);

}


$images_uploaded = array();
function upload_file($image) {
    global $images_uploaded;

    if(isset($images_uploaded[md5($image)])) return $images_uploaded[md5($image)];

    // actually upload the file
    $tmp_file = tempnam(sys_get_temp_dir(), 'Harvest');
    $copy = copy($image, $tmp_file);

    $file_array = array(
        'name' => basename($image), // ex: wp-header-logo.png
        'type' => 'image/jpg',
        'tmp_name' => $tmp_file,
        'error' => 0,
        'size' => filesize($image),
    );


    $overrides = array(
        // tells WordPress to not look for the POST form
        // fields that would normally be present, default is true,
        // we downloaded the file from a remote server, so there
        // will be no form fields
        'test_form' => false,

        // setting this to false lets WordPress allow empty files, not recommended
        'test_size' => true,

        // A properly uploaded file will pass this test.
        // There should be no reason to override this one.
        'test_upload' => true,
    );

    $file = wp_handle_sideload( $file_array, $overrides );
    if ( isset($file['error']) ) {
        $error = new WP_Error('upload_error', $file['error']);
        print_r($error);
        echo "\n" . $image . "\n";
        exit;
    }

    $url = $file['url'];
    $type = $file['type'];
    $file = $file['file'];
    $title = preg_replace('/\.[^.]+$/', '', basename($file));
    $content = '';

    // Use image exif/iptc data for title and caption defaults if possible.
    if ( $image_meta = @wp_read_image_metadata($file) ) {
        if ( trim( $image_meta['title'] ) && ! is_numeric( sanitize_title( $image_meta['title'] ) ) )
            $title = $image_meta['title'];
        if ( trim( $image_meta['caption'] ) )
            $content = $image_meta['caption'];
    }

    if ( isset( $desc ) )
        $title = $desc;

    // Construct the attachment array.
    $attachment = array(
        'post_mime_type' => $type,
        'guid' => $url,
        'post_title' => $title,
        'post_content' => $content,
    );

    // This should never be set as it would then overwrite an existing attachment.
    if ( isset( $attachment['ID'] ) )
        unset( $attachment['ID'] );

    // Save the attachment metadata
    $thumbnail_id = wp_insert_attachment($attachment, $file);
    if ( !is_wp_error($thumbnail_id) )
        wp_update_attachment_metadata( $thumbnail_id, wp_generate_attachment_metadata( $thumbnail_id, $file ) );


    $images_uploaded[md5($image)] = $thumbnail_id;

    return $thumbnail_id;

}
function get_product_by_sku( $sku ) {

    global $wpdb;

    $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $sku ) );

    if ( $product_id ) return $product_id;

    return null;

}