<?php

namespace Roots\Sage;

class Option {

	private static $cached = [];
	private static $wp_cached = [];

	public static function wp($key) {

		if(empty(self::$wp_cached[$key]))
			self::$wp_cached[$key] = get_bloginfo($key);


		return self::$wp_cached[$key];

	}

	public static function get($key) {

		if(empty(self::$cached[$key]))
			self::$cached[$key] = get_field($key, 'option');


		return self::$cached[$key];

	}

	public static function base($path = "") {

		$key = 'base_url';
		if(empty(self::$cached[$key]))
			self::$cached[$key] = get_bloginfo('url');


		return self::$cached[$key] . $path;

	}

}