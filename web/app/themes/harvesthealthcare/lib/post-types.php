<?php
/**
 * @package aureo
 * @copyright Core Creative 2015
 * @author Oliver Sadler (www.oli.digital)
 * @author Core Creative (www.corecreative.co.uk)
 */


add_action( 'init', function() {

	register_post_type( 'post', array(
		'labels' => array(
			'name_admin_bar' => _x( 'Post', 'add new on admin bar' ),

			'name' => 'Latest News',
			'singular_name' => 'Post',
			'menu_name' => 'Posts',
		),
		'public'  => true,
		'_builtin' => true, /* internal use only. don't use this when registering your own post type. */
		'_edit_link' => 'post.php?post=%d', /* internal use only. don't use this when registering your own post type. */
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => true,
		'has_archive' => 'news',
		'query_var' => true,
		'delete_with_user' => true,
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'post-formats' ),
	) );

	$labels = array(
		'name' => _x( 'Testimonials', 'testimonial' ),
		'singular_name' => _x( 'Testimonial', 'testimonial' ),
		'add_new' => _x( 'Add New', 'testimonial' ),
		'add_new_item' => _x( 'Add New Testimonial', 'testimonial' ),
		'edit_item' => _x( 'Edit Testimonial', 'testimonial' ),
		'new_item' => _x( 'New Testimonial', 'testimonial' ),
		'view_item' => _x( 'View Testimonial', 'testimonial' ),
		'search_items' => _x( 'Search Testimonials', 'testimonial' ),
		'not_found' => _x( 'No testimonials found', 'testimonial' ),
		'not_found_in_trash' => _x( 'No testimonials found in Trash', 'testimonial' ),
		'parent_item_colon' => _x( 'Parent Testimonial:', 'testimonial' ),
		'menu_name' => _x( 'Testimonials', 'testimonial' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,

		'supports' => array( 'title', 'editor' ),

		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,

		'menu_icon' => 'dashicons-awards',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => true,
		'has_archive' => false,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => false,
		'capability_type' => 'post'
	);

	register_post_type( 'testimonial', $args );


	$labels = array(
		'name' => _x( 'Case Studies', 'case_study' ),
		'singular_name' => _x( 'Case Study', 'case_study' ),
		'add_new' => _x( 'Add New', 'case_study' ),
		'add_new_item' => _x( 'Add New Case Study', 'case_study' ),
		'edit_item' => _x( 'Edit Case Study', 'case_study' ),
		'new_item' => _x( 'New Case Study', 'case_study' ),
		'view_item' => _x( 'View Case Study', 'case_study' ),
		'search_items' => _x( 'Search Case Studies', 'case_study' ),
		'not_found' => _x( 'No case studies found', 'case_study' ),
		'not_found_in_trash' => _x( 'No case studies found in Trash', 'case_study' ),
		'parent_item_colon' => _x( 'Parent Case Study:', 'case_study' ),
		'menu_name' => _x( 'Case Studies', 'case_study' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' => false,

		'supports' => array( 'title', 'editor', 'thumbnail' ),

		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,

		'menu_icon' => 'dashicons-analytics',
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => 'case-studies',
		'query_var' => true,
		'can_export' => true,
		'rewrite' => array(
			'slug' => 'case-study'
		),
		'capability_type' => 'post'
	);

	register_post_type( 'case_study', $args );

}, 1);