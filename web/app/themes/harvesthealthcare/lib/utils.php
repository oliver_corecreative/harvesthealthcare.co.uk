<?php

namespace Roots\Sage\Utils;

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form()
{
    $form = '';
    locate_template('/templates/blocks/searchform.php', true, false);
    return $form;
}

add_filter('get_search_form', __NAMESPACE__ . '\\get_search_form');

function hide_email($email) {
    $character_set = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
    $key = str_shuffle($character_set);
    $cipher_text = '';
    $id = 'e' . rand(1, 999999999);

    for ($i = 0; $i < strlen($email); $i += 1) {
        $cipher_text .= $key[strpos($character_set, $email[$i])];
    }

    $script = 'var a="' . $key . '";var b=a.split("").sort().join("");var c="' . $cipher_text . '";var d="";';
    $script .= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
    $script .= 'document.getElementById("' . $id . '").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';

    $script = "eval(\"" . str_replace(array("\\", '"'), array("\\\\", '\"'), $script) . "\")";
    $script = '<script type="text/javascript">/*<![CDATA[*/' . $script . '/*]]>*/</script>';

    return '<span id="' . $id . '">[protected email address]</span>' . $script;

}

function arrayToSelect( $array, $selected = ''){

    $output = "";

    foreach($array as $value=>$text){

        if(is_int($value))
            $value = $text;

        if(is_array($text)){

            $output .= '<optgroup id="'.$value.'" label="'.$text['label'].'">';
            unset($text['label']);
            $output .= arrayToSelect($text, $selected); //YAY RECURSIVE!!
            $output .= '</optgroup>' . "\n";

        }else{

            $output .= '<option title="'.$text.'" value="'.$value.'" ';
            if($value == $selected){

                $output .= 'selected=selected';

            }
            $output .=	' >'.$text.'</option>' . "\n";

        }

    }
    return ($output);

}