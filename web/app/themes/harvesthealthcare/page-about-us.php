<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<section class="grey-bg block mission-statement">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h4>Mission Statement</h4>
                <div class="entry-content">
                    <?php the_field('mission_statement'); ?>
                </div>
            </div>
        </div>
    </div>
</section>



<?php
$items = get_field("usps");
if(!empty($items)):
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h3 class="bar bar-primary text-center">What makes us special</h3>
        </div>
    </div>
</div>

<section class="usps">
    <div class="container">
        <div class="row">
            <?php foreach($items as $item): ?>
                <div class="col-xs-12 col-md-6">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" src="<?= $item['icon']; ?>" />
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading text-light"><?= $item['title']; ?></h5>
                            <div class="entry-content">
                                <?= $item['content']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>