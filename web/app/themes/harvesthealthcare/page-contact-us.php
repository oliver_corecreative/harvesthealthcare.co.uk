<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8">

            <?= CC_Form::display_form('contact'); ?>

        </div>
        <div class="col-xs-12 col-sm-4 sidebar">

            <?php dynamic_sidebar('sidebar-contact'); ?>

        </div>
    </div>
</div>

<section class="map block block-small">
    <span class="hide" id="address"><?= \Roots\Sage\Option::get('address'); ?></span>
    <div id="map-container"></div>
</section>

<?php get_template_part("templates/blocks/usps"); ?>