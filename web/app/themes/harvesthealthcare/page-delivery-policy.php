<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<section class="block grey-bg map">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">

                <h2 class="h1 entry-title">Worldwide Delivery Policy</h2>
                <div class="entry-content large">
                    <p>We currently have distributors in South Africa, Australia, New Zealand, Germany, Dubai and are continually looking to expand this network to include new and developing markets</p>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <img src="<?= \Roots\Sage\Assets\asset_path('images/delivery-map.png'); ?>" class="img-responsive" alt="Worldwide Distributor Locations" />
            </div>
            <div class="col-xs-12 col-md-6">

                <div class="entry-content">

                    <h5>Can't see your country on our list?</h5>
                    <p>Call our UK based sales team today on +44 (0)1709 377173 and we can look into developing our market to your country.</p>
                    <p>Alternatively, fill in the form on our contact page and we will get back to you as soon as possible. </p>

                </div>
                <a href="/contact-us" class="btn btn-primary btn-wide text-uppercase">Contact Us</a>

            </div>
        </div>
    </div>
</section>

<?php get_template_part("templates/blocks/usps"); ?>
