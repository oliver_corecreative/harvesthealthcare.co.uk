<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<div class="container">
    <section class="download-catalogue">
        <div class="row">
            <div class="col-xs-12 col-lg-6 col-md-8 col-md-offset-4 col-lg-offset-6">

                <div class="entry">
                    <div class="entry-content">
                        <h2>Download our latest Harvest catalogue…</h2>
                        <p>Our latest catalogue is now available to be downloaded click the link below to download your very own today…</p>
                    </div>

                    <a class="btn btn-primary" href="/product-catalogue" target="_blank">DOWNLOAD NOW</a>
                </div>

            </div>
        </div>
    </section>
</div>

<?php
$rows = get_field('blocks');
if(!empty($rows)): ?>
<section class="download-sections">
    <form class="other-downloads" action="<?= admin_url('admin-ajax.php'); ?>" method="post">
        <input type="hidden" name="action" value="downloads_other" />

        <?php foreach($rows as $row): ?>
            <section class="item block block-small">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h3 class="bar bar-secondary text-lowercase"><?= $row['name']; ?></h3>
                        </div>
                    </div>
                    <?php if(!empty($row['files'])): ?>
                        <div class="row">

                            <?php foreach($row['files'] as $file): ?>
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-24 form-group">
                                <label>
                                    <div class="image">
                                        <?php if(!empty($file['brochure_url'])): ?><a href="<?= $file['brochure_url']; ?>" target="_blank" title="View online brochure" class="hover-opacity"><?php endif; ?>
                                        <img src="<?= \Roots\Sage\App::image($file['cover'], 300, 300); ?>" class="img-responsive" />
                                        <?php if(!empty($file['brochure_url'])): ?></a><?php endif; ?>
                                    </div>
                                    <!--<input type="checkbox" name="files[]" value="<//?= $file['file']['id']; ?>" class="fancy" />-->
                                    <?= $file['file']['title']; ?>
                                </label>
                            </div>
                            <?php endforeach; ?>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-4 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Download Selected</button>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <p>There are no downloads available at this time. Please check back later.</p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        <?php endforeach; ?>
        <?php
        $videos = get_field('videos');
        if(!empty($videos)): ?>
        <section class="item block block-small videos">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="bar bar-secondary text-lowercase">Technical Video</h3>
                    </div>
                </div>
                <div class="row">
                    <?php foreach($videos as $video): ?>
                        <?php
                        parse_str(parse_url($video['url'], PHP_URL_QUERY), $parts);
                        $id = $parts['v'];
                        ?>
                        <div class="col-xs-12 col-sm-4 text-center">
                            <a href="<?= $video['url']; ?>" class="item" rel="prettyPhoto">
                                <div class="image">
                                    <img src="<?= \Roots\Sage\App::image('http://img.youtube.com/vi/' . $id . '/hqdefault.jpg', 450, 254, 1); ?>" class="img-responsive" />
                                    <i class="icon icon-play"></i>
                                </div>
                                <div class="entry-content"><?= $video['title']; ?></div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <?php endif; ?>

        <section class="block grey-bg form">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-center">
                        <h2 class="h1">Request a printed version of our Latest Catalogue</h2>
                    </div>
                </div>
                <?= CC_Form::display_form('download-catalogue'); ?>
            </div>
        </section>

    </form>
</section>
<?php endif; ?>

<?php get_template_part("templates/blocks/usps"); ?>