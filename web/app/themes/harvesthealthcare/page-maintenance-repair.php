<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<section class="grey-bg block servicing">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 text-center">
                <img src="<?= get_field('servicing_logo'); ?>" alt="harvestservicing" />
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="entry-content">
                    <?= get_field('servicing_section'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="blocks">

    <div class="container">
        <div class="row">

            <?php foreach(get_field('blocks') as $block): ?>
            <div class="<?= $block['style']; ?>">
                <h3 class="bar bar-secondary standard-case"><?= $block['heading']; ?></h3>
                <div class="entry-content">
                    <?= $block['content']; ?>
                </div>
            </div>
            <?php endforeach; ?>

        </div>
    </div>

</section>

<section class="grey-bg block dbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="entry-content text-center">
                    <?= get_field('dbs_section'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="block block-small form">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="entry-content text-center">
                    <p>If you require a quote or any information please fill out the form below and we will get back to you in 24 hours.<br />
                        Alternatively, you can call us on our free phone: <?= \Roots\Sage\Option::get('phone_number'); ?></p>
                </div>
            </div>
        </div>
        <?= CC_Form::display_form('maintenance'); ?>
    </div>
</section>

<section class="grey-bg block block-wrapper">
    <?php get_template_part('templates/blocks/link-blocks'); ?>
</section>


<?php get_template_part("templates/blocks/usps"); ?>