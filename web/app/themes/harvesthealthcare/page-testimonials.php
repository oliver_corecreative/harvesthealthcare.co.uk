<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>

        </div>
    </div>
</div>

<?php
//get all gallery images
$posts = \Roots\Sage\App::get_posts([
    'post_type' => 'testimonial'
]);

$images = array(

);

$chunks = array_chunk($posts, 2);
$row_num = 0;
$rows = array();
foreach($chunks as $row) {
    $row_num++;
    if($row_num == 3) $row_num = 0;
    $split_in = (ceil(rand(1, 6))/2)-1;

    array_splice( $row, $split_in, 0, array( 'image' => $row_num+1 ) ); // splice in at position 3
    $rows[] = $row;
}

?>

<section class="testimonials archive">

    <div class="container">
        <?php foreach($rows as $row): ?>
            <div class="row">

                <?php foreach($row as $item): ?>
                    <div class="col-xs-12 col-md-4">
                        <?php if(is_int($item)): ?>
                            <div class="image">
                                <?php
                                $url = Roots\Sage\Assets\asset_path('images/testimonials/Testimonials-'.$item.'.png');
                                ?>
                                <img src="<?= $url; ?>" class="img-responsive" />
                            </div>
                        <?php else: ?>
                            <div class="<?= rand(0,1) == 1 ? 'secondary-bg' : ''; ?> item">
                                <table width="100%" height="199">
                                    <tr>
                                        <td align="center" valign="middle">
                                            <div class="entry-content">
                                                <?= $item->post_content; ?>
                                                <div class="entry-title"><strong><?= $item->post_title; ?></strong></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>

            </div>
        <?php endforeach; ?>
    </div>

</section>

<?php get_template_part("templates/blocks/usps"); ?>