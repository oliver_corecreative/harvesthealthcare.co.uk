<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-4 col-md-9 col-md-push-3">

            <?php get_template_part('templates/content-single', get_post_type()); ?>

        </div>
        <div class="col-xs-12 col-sm-4 col-sm-pull-8 col-md-3 col-md-pull-9 sidebar">

            <?php get_template_part('templates/sidebar'); ?>

        </div>
    </div>
    <?php if(get_post_type() == 'case_study'): ?>
        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-push-4">
                <a href="/case-studies/" class="btn btn-block btn-primary text-uppercase">View all Case Studies</a>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php get_template_part('templates/blocks/link-blocks'); ?>