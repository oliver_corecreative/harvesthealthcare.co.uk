<?php
/**
 * Template Name: Custom Template
 */
?>
<div class="container entry entry-main">
    <div class="row">
        <div class="col-xs-12 text-left">

            <?php while (have_posts()) : the_post(); ?>
                <?php get_template_part('templates/page', 'header'); ?>
                <div class="entry-content text-left">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; ?>

        </div>
    </div>
</div>