<?php
/**
 * Template Name: Embed Template
 */
?>
<?php
use Roots\Sage\Config;
use Roots\Sage\Wrapper;
?>
<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

<?php the_content(); ?>

<?php wp_footer(); ?>
</body>
</html>

