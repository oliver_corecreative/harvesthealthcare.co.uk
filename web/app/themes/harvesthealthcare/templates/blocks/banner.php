<?php


//$show_banner = get_field('show_banner');
$banner_slides = apply_filters('banner_slides', get_field('banner_slides'));

$show_banner = !empty($banner_slides);

$banner_height = apply_filters('banner_height', get_field('banner_height'));
?>

<section class="banner <?= !$show_banner ? 'no-banner' : ''; ?>">


    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <nav class="collapse navbar-collapse" role="navigation">
                <div class="filler"></div>
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new Roots\Sage\Nav\NavWalker(), 'menu_class' => 'nav navbar-nav']);
                endif;
                ?>
            </nav>
        </div>
    </div>


    <?php if($show_banner): ?>
    <div class="wrapper">


        <?php foreach($banner_slides as $slide): ?>
            <div class="slide">
                <div class="container">
                    <div class="row" style="min-height: <?= $banner_height; ?>px;">
                        <div class="col-xs-12 col-sm-6">

                            <div class="media">
                                <?php if($slide['show_button']): ?>
                                    <div class="media-left">

                                        <a href="<?= $slide['button_url']; ?>" class="category-label">
                                            <span><?= $slide['button_text']; ?></span>
                                        </a>

                                    </div>
                                <?php endif; ?>
                                <div class="media-body">
                                    <h4 class="entry-title strong">
                                        <?= $slide['heading_text']; ?>
                                    </h4>
                                    <?php if(!empty($slide['link_text']) && !empty($slide['link_url'])): ?>
                                        <a href="<?= $slide['link_url']; ?>" class="text-light text-uppercase text-large color-inherit"><?= $slide['link_text']; ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>

                        </div>
                        <?php if($slide['image']): ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="image-area" style="min-height: <?= $banner_height; ?>px;">
                                <?php
                                $image_height = $slide['image']['height'];

                                if($image_height > $banner_height) {
                                    $style = 'height: ' . $banner_height . 'px; width: auto;';
                                }

                                if($slide['image_position_vertical'] == 'center' && $image_height <= $banner_height) {

                                        $offset = ($banner_height - $image_height) / 2;
                                        $style = 'margin-top: '.$offset.'px;';

                                } elseif($slide['image_position_vertical'] == 'bottom' && $image_height <= $banner_height) {

                                    $offset = ($banner_height - $image_height) - $slide['image_position_vertical_offset'];
                                    $style = 'margin-top: '.$offset.'px;';

                                } else {
                                    $style = 'margin-top: '.$slide['image_position_vertical_offset'].'px;';
                                }
                                $style .= 'margin-right: ' . $slide['image_position_horizontal_offset'] . 'px;';

                                ?>
                                <img src="<?= $slide['image']['url']; ?>" class="img-responsive" style="<?= $style; ?>" />
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>

    </div>
    <aside class="banner-pagination"><div class="container"></div></aside>
    <?php endif; ?>

</section>

<?php get_template_part('templates/blocks/team-tab'); ?>