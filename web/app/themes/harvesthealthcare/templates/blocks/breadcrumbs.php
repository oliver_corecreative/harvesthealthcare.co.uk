<section class="breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 inner">
                <?php if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb('<p id="breadcrumbs">','</p>');
                } ?>
                <a href="javascript:history.go(-1);" class="back">&lt; Go Back</a>
            </div>
        </div>
    </div>
</section>
