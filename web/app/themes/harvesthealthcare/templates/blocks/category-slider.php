<?php
$categories = \Roots\Sage\App::get_main_categories(array( 'orderby' => 'rand' ));
if(empty($categories)) return $categories;
?>
<section class="categories slider">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">
                <div class="bar bar-secondary">
                    <h2 class="pull-left">Our products at a glance</h2>
                    <a href="<?= get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="pull-right">view all</a>
                </div>
            </div>

        </div>
        <div class="slider-container">
            <a href="#" class="pager prev">
                <i class="icon icon-arrow-left"></i>
            </a>
            <div class="wrapper">
                <div class="row">

                    <?php
                    $i = 0; foreach ( $categories as $category ) {
                        wc_get_template( 'content-product_cat.php', array(
                            'category' => $category
                        ) );
                        if($i % 3 == 2) {
                            echo '</div><div class="row">';
                        }
                        $i++; }
                    ?>

                </div>
            </div>
            <a href="#" class="pager next">
                <i class="icon icon-arrow-right"></i>
            </a>
        </div>
    </div>
</section>