<?php $items = YITH_Request_Quote()->get_raq_item_number(); ?>
<div class="enquiry-basket-item text-right">
    <a href="/request-quote/"><img src="<?= \Roots\Sage\Assets\asset_path("images/basket.svg"); ?>" /> Your enquiry basket has <?= $items; ?> <?= _n("item", "items", $items); ?> <i class="icon icon-arrow-right"></i></a>
</div>