<?php $blocks = Roots\Sage\Option::get('footer_links'); ?>
<section class="block footer-links">
    <div class="container">
        <div class="row">

            <?php foreach($blocks as $block): ?>
            <div class="col-xs-12 col-sm-4 entry text-center">
                <a href="<?= $block['link_url']; ?>">
                    <h3 class="entry-title"><?= $block['title']; ?></h3>
                    <div class="link">
                        <img src="<?= $block['image']['url']; ?>" class="img-responsive" alt="<?= $block['title']; ?>" />
                        <div class="overlay">
                            <div class="triangle"></div>
                            <span><?= $block['link_text']; ?></span>
                        </div>
                    </div>
                    <div class="entry-content">
                        <p><?= $block['content']; ?></p>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>

        </div>
    </div>
</section>