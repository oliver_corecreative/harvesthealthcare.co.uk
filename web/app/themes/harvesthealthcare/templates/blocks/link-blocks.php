<?php
$items = \Roots\Sage\Option::get('link_blocks');
if(empty($items)) return;
?>
<section class="link-blocks">
    <div class="container">
        <div class="row">
        <?php foreach($items as $item): $item['title'] = do_shortcode($item['title']); ?>
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="link-block">
                    <a href="<?= $item['link']; ?>">
                        <h3 class="strong"><?= $item['title']; ?></h3>
                        <i class="icon icon-arrow-right contained-primary"></i>

                        <img src="<?= $item['image']; ?>" alt="<?= strip_tags($item['title']); ?>" />
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</section>