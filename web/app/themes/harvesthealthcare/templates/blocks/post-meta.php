<aside class="meta">


    <?php if(get_post_type() == 'case_study'): ?>

        <span>
            <i class="icon icon-document"></i>
            Case Study <?= str_pad($wp_query->current_post+1, 3, '0', STR_PAD_LEFT); ?>
        </span>
        <span>
            <i class="icon icon-pencil"></i>
            <time class="updated" datetime="<?= get_the_time('c'); ?>"><?= get_the_date('jS F Y'); ?></time>
        </span>

    <?php else: ?>

        <span>
            <i class="icon icon-user"></i>
            <?= sprintf( '%s', get_the_author() ); ?>
        </span>
        <span>
            <i class="icon icon-pencil"></i>
            <time class="updated" datetime="<?= get_the_time('c'); ?>"><?= get_the_date('jS F Y'); ?></time>
        </span>
        <span>
            <i class="icon icon-list"></i>
            <?php the_category(', '); ?>
        </span>
        <?php if ( comments_open() ) : ?>
            <span class="pull-right">
                <i class="icon icon-comment"></i>
                <span class="comments-link">
                    <?php comments_popup_link( 'Leave a reply', '1 Comment', '% Comments' ); ?>
                </span><!-- .comments-link -->
            </span>
        <?php endif; // comments_open() ?>

    <?php endif; ?>

</aside>