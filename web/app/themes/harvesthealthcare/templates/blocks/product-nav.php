<div class="dropdown-menu">
    <nav class="categories container-fluid">
        <div class="row">

            <?php
            $menu = array();

            $args = array(
                'taxonomy' => 'product_cat',
                'title_li' => false,
                'hide_empty' => 0,
                'parent'     => '',
                'number'     => null,
                'orderby'    => 'name',
                'order'      => 'ASC',
                'echo' => 0
            );

            $items = get_terms($args['taxonomy'], $args);

            foreach($items as $item) {
                $item->link = esc_url(get_term_link($item));

                $short_title = get_field('short_title', $item);
                $item->short_title = empty($short_title) ? $item->name : $short_title;

                $menu[$item->parent][] = $item;
            }

            $columns = Roots\Sage\Extras\array_divide($menu[0], 4);

            ?>

            <?php foreach($columns as $items): ?>
                <div class="col-xs-12 col-md-3">
                    <?php foreach($items as $item): ?>
                        <ul class="list-unstyled">
                            <li class="heading">
                                <a href="<?= $item->link; ?>"><?= $item->short_title; ?></a>
                            </li>
                            <?php if(!empty($menu[$item->term_id])): foreach($menu[$item->term_id] as $child): ?>
                                <li>
                                    <a href="<?= $child->link; ?>"><?= $child->name; ?></a>
                                </li>
                            <?php endforeach; endif; ?>
                        </ul>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>

        </div>
    </nav>
</div>