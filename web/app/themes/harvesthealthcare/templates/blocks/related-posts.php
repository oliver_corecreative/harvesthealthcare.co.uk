<?php
$plugin_defaults = Related_Posts_By_Taxonomy_Defaults::get_instance();

$rpbt_args = array(
    // shortcode defaults
    'post_id' => '', 'taxonomies' => $plugin_defaults->all_tax,
    'before_shortcode' => '<div class="rpbt_shortcode">', 'after_shortcode' => '</div>',
    'before_title' => '<h3>', 'after_title' => '</h3>',
    'title' => __( 'Related Posts', 'related-posts-by-taxonomy' ),
    'format' => 'links',
    'image_size' => 'thumbnail', 'columns' => 3,
    'caption' => 'post_title',

    // km_rpbt_related_posts_by_taxonomy defaults
    'post_types' => '', 'posts_per_page' => 5, 'order' => 'DESC',
    'limit_posts' => -1, 'limit_year' => '',
    'limit_month' => '', 'orderby' => 'post_date',
    'exclude_terms' => '', 'include_terms' => '',  'exclude_posts' => '',
    'related' => '', // 'post_thumbnail' => '', 'fields' => 'all'
);

if ( '' === trim( $rpbt_args['post_id'] ) ) {
    $rpbt_args['post_id'] = get_the_ID();
}

/* if no post type is set use the post type of the current post (new default since 0.3) */
if ( '' === trim( $rpbt_args['post_types'] ) ) {
    $post_types = get_post_type( $rpbt_args['post_id'] );
    $rpbt_args['post_types'] = ( $post_types ) ? $post_types : 'post';
}

if ( $rpbt_args['taxonomies'] === $plugin_defaults->all_tax ) {
    $rpbt_args['taxonomies'] = array_keys( $plugin_defaults->taxonomies );
}

// convert 'related' string to boolean.
$rpbt_args['related'] = ( '' !== trim( $rpbt_args['related'] ) ) ? $rpbt_args['related'] : true;
$rpbt_args['related'] = filter_var( $rpbt_args['related'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );

// non boolean used, default to true
$rpbt_args['related'] = ( $rpbt_args['related'] === NULL ) ? true : $rpbt_args['related'];

$function_args = $rpbt_args;

/* restricted arguments */
unset( $function_args['post_id'], $function_args['taxonomies'], $function_args['fields'] );

$related_posts = km_rpbt_related_posts_by_taxonomy( $rpbt_args['post_id'], $rpbt_args['taxonomies'], $function_args );

if(!empty($related_posts)):

$post_types = get_post_types(array(), 'objects');

$label = $post_types[get_post_type()]->labels->name;

?>
<section class="related-posts <?= get_post_type(); ?>">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <h3 class="standard-case">Related <?= $label; ?>...</h3>
            </div>
        </div>
        <div class="row">
            <?php

            foreach($related_posts as $post) {
                setup_postdata($post);

                get_template_part('templates/content', get_post_type());

            }
            wp_reset_postdata();

            ?>
        </div>
    </div>
</section>
<?php endif; ?>