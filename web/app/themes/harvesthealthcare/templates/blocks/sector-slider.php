<section class="sector slider">

    <?php $sectors = Roots\Sage\App::get_main_categories(array( 'taxonomy' => 'sector' )); ?>

    <header>
        <div class="container">

            <?php $i = 1; foreach($sectors as $item): ?>
            <div class="item pull-left text-center" style="width: <?= floor(100/count($sectors)); ?>%">
                <a href="#"><span><?= $i; ?></span></a>
            </div>
            <?php $i++; endforeach; ?>

        </div>
    </header>

    <div class="wrapper">

        <?php $i = 1; foreach($sectors as $item): ?>
        <article title="<?= $item->name; ?>">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 col-md-6 text-right">
                        <h3 class="entry-title">
                            <?= $item->name; ?><br />
                            <span class="brand-primary"><?= $item->sub_title; ?></span>
                        </h3>
                        <?php if(!empty($item->image['src'])): ?>
                            <a href="<?= $item->permalink; ?>" class="thumbnail">
                                <img src="<?= $item->image['src']; ?>" width="<?= $item->image['width']; ?>" height="<?= $item->image['height']; ?>" alt="<?= $item->name; ?>" title="<?= $item->name; ?>" />
                            </a>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-7 col-md-6 entry">
                        <div class="entry-content"><?= $item->description; ?></div>
                        <a href="<?= $item->permalink; ?>" class="btn btn-primary btn-wide">View Full Range</a>
                    </div>
                </div>
            </div>
        </article>
        <?php $i++; endforeach; ?>

    </div>

</section>