<div class="social-icons">
    <?php
    $links = array(
        'linkedin' => 'https://www.linkedin.com/company/harvest-healthcare-ltd',
        'twitter' => 'https://twitter.com/HarvestHealthUK',
        'facebook' => 'https://www.facebook.com/HarvestHealthcare/',
        'googleplus' => 'https://plus.google.com/104796758355192702114/',
        'youtube' => 'https://www.youtube.com/channel/UC0CKvEHYnFhPzqRu8OtjsZA'
    );
    ?>
    <?php foreach($links as $icon => $url): ?>
    <a href="<?= $url; ?>" target="_blank"><i class="icon social icon-<?= $icon; ?>"></i></a>
    <?php endforeach; ?>
</div>