<?php
use Roots\Sage\App as App;
?>
<section class="testimonials block grey-bg">

    <div class="container">
        <!--<div class="filler"></div>-->
        <div class="row">
            <div class="col-xs-12 col-md-4 cell text-right">
                <h2 class="strong">What our customers say</h2>
            </div>
            <div class="col-xs-12 col-md-8 cell slider-container">

                <div class="wrapper">
                    <?php $i = 0; foreach(App::get_posts([ 'post_type' => 'testimonial' ]) as $testimonial): ?>
                        <article title="<?= $testimonial->post_title; ?>">
                            <div class="entry-content">
                                <?= $testimonial->post_content; ?>
                            </div>
                            <strong><?= $testimonial->post_title; ?></strong>
                        </article>
                        <?php $i++; endforeach; ?>
                </div>

            </div>
        </div>
    </div>

</section>