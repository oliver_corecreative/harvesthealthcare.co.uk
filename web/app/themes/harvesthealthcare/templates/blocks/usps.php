<?php
$items = \Roots\Sage\Option::get("usps");
if(empty($items)) return;
?>
<section class="usps">
    <div class="container">
        <div class="row">
            <?php foreach($items as $item): ?>
                <div class="col-xs-12 col-md-4">
                    <div class="media">
                        <div class="media-left">
                            <img class="media-object" src="<?= $item['icon']; ?>" />
                        </div>
                        <div class="media-body">
                            <h5 class="media-heading text-light"><?= $item['title']; ?></h5>
                            <div class="entry-content">
                                <?= $item['content']; ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>