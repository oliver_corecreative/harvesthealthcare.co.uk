<?php
if (post_password_required()) {
    return;
}

if(comments_open()):
?>

<hr />

<section id="comments" class="comments">
    <h4 class="comment-title">Leave a reply...</h4>
    <?php if (have_comments()) : ?>

        <ol class="media-list">
            <?php wp_list_comments(array('walker' => new Roots_Walker_Comment)); ?>
        </ol>

        <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : ?>
            <nav>
                <ul class="pager">
                    <?php if (get_previous_comments_link()) : ?>
                        <li class="previous"><?php previous_comments_link(__('&larr; Older comments', 'roots')); ?></li>
                    <?php endif; ?>
                    <?php if (get_next_comments_link()) : ?>
                        <li class="next"><?php next_comments_link(__('Newer comments &rarr;', 'roots')); ?></li>
                    <?php endif; ?>
                </ul>
            </nav>
        <?php endif; ?>

        <?php if (!comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
            <div class="alert alert-warning">
                <?php _e('Comments are closed.', 'roots'); ?>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <p>There are no comments yet. Be the first one:</p>
    <?php endif; ?>
</section><!-- /#comments -->

<?php if (!have_comments() && !comments_open() && !is_page() && post_type_supports(get_post_type(), 'comments')) : ?>
    <section id="comments">
        <div class="alert alert-warning">
            <?php _e('Comments are closed.', 'roots'); ?>
        </div>
    </section><!-- /#comments -->
<?php endif; ?>

    <section id="respond">
        <h2 class="hide"><?php comment_form_title(__('Leave a Reply', 'roots'), __('Leave a Reply to %s', 'roots')); ?></h2>
        <p class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></p>
        <?php if (get_option('comment_registration') && !is_user_logged_in()) : ?>
            <p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.', 'roots'), wp_login_url(get_permalink())); ?></p>
        <?php else : ?>
            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
                <?php if (is_user_logged_in()) : ?>
                    <p>
                        <?php printf(__('Logged in as <a href="%s/wp-admin/profile.php">%s</a>.', 'roots'), get_option('siteurl'), $user_identity); ?>
                        <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'roots'); ?>"><?php _e('Log out &raquo;', 'roots'); ?></a>
                    </p>
                <?php else : ?>
                    <div class="form-group">
                        <label for="author" class="hide"><?php _e('Name', 'roots'); if ($req) _e(' *', 'roots'); ?></label>
                        <input type="text" name="author" class="form-control" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" <?php if ($req) echo 'aria-required="true"'; ?> placeholder="<?php _e('Your name', 'roots'); if ($req) _e(' *', 'roots'); ?>">
                    </div>
                    <div class="form-group">
                        <label for="email" class="hide"><?php _e('Email', 'roots'); if ($req) _e(' *', 'roots'); ?></label>
                        <input type="email" name="email" class="form-control" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" <?php if ($req) echo 'aria-required="true"'; ?> placeholder="<?php _e('Your email', 'roots'); if ($req) _e(' *', 'roots'); ?>">
                    </div>
                <?php endif; ?>
                <div class="form-group">
                    <label for="comment" class="hide"><?php _e('Comment', 'roots'); ?></label>
                    <textarea name="comment" class="form-control" id="comment" rows="5" aria-required="true" placeholder="Leave a reply..."></textarea>
                </div>
                <p>Your email address will not be published. Required fields are marked *</p>
                <p><button name="submit" class="btn btn-wide btn-primary btn-large" type="submit" id="submit"><?php _e('Post Comment', 'roots'); ?></button></p>
                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', $post->ID); ?>
            </form>
        <?php endif; ?>
    </section><!-- /#respond -->
<?php endif; ?>
