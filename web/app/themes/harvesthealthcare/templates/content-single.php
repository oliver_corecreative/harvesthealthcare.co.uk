<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('content'); ?>>

        <header>

            <?php get_template_part('templates/page', 'header-bar'); ?>
            <?php get_template_part('templates/blocks/post-meta'); ?>

        </header>

        <div class="entry-content">
            <?php the_content(); ?>
        </div>

        <footer>
            <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
        </footer>

        <?php comments_template('/templates/comments.php'); ?>

    </article>
<?php endwhile; ?>