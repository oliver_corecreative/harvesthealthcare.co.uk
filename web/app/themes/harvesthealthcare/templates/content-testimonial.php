<article <?php post_class( 'text-center' ); ?>>
    <div class="entry-content strong"><?php the_content(); ?></div>
    <h4 class="entry-title brand-primary strong"><?php the_title(); ?></h4>
</article>
