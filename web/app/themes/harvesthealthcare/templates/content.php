<article <?php post_class('content'); ?>>

    <!-- meta bar -->
    <header>

    <?php get_template_part('templates/blocks/post-meta'); ?>

    <a href="<?php the_permalink(); ?>">
        <h3 class="entry-title"><?php the_title(); ?></h3>
    </a>

    </header>

    <div class="entry-excerpt entry-content">
        <?php the_excerpt(); ?>
    </div>

    <div class="entry-media">
        <?php if(has_post_thumbnail()): ?>
        <a href="<?php the_permalink(); ?>" class="entry-image">
            <?php the_post_thumbnail('large', array( 'class' => 'img-responsive attachment-large hover-opacity' )); ?>
        </a>
        <?php endif; ?>

        <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-icon">
            Read more
            <i class="icon icon-arrow-right"></i>
        </a>
    </div>

</article>
