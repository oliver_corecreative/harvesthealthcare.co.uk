<?php //if(!is_page('about-us')) get_template_part('templates/blocks/footer', 'links'); ?>

<footer class="content-info" role="contentinfo">
    <div class="container">
        <?php dynamic_sidebar('sidebar-footer'); ?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-6 col-lg-5">
                <h6>Product Categories</h6>
                <ul class="list-unstyled two-col">
                    <?php foreach(Roots\Sage\App::get_main_categories() as $category): ?>
                        <li><a href="<?= $category->permalink; ?>" title="<?= $category->name; ?>"><?= $category->name; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2">
                <h6>Information</h6>
                <?php
                if (has_nav_menu('footer_navigation')):
                    wp_nav_menu(['theme_location' => 'footer_navigation', 'walker' => new Roots\Soil\Nav\NavWalker(), 'menu_class' => 'list-unstyled']);
                endif;
                ?>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-3 col-lg-5 logo logo-white">
                <a class="brand hover-opacity" href="<?= Roots\Sage\Option::base(); ?>">
                    <img src="<?= \Roots\Sage\Assets\asset_path("images/logo-white.svg"); ?>" alt="<?= Roots\Sage\Option::wp('name'); ?>" width="170" />
                </a>
                <div class="clearfix"></div>
                <div class="text-right">
                    <?php get_template_part("templates/blocks/social-icons"); ?>
                </div>
            </div>
            <div class="clearfix hidden-sm"></div>

            <div class="col-xs-12 col-sm-7 col-md-5 subscribe-wrapper pull-right">
                <p class="small">Get the latest news and offers direct to your inbox, sign up today</p>
                <?= \CC_Campaign_Monitor::template(); ?>
                <div class="contact-details"><strong>Call</strong> <a href="tel:<?= Roots\Sage\Option::get('phone_number'); ?>"><?= Roots\Sage\Option::get('phone_number'); ?></a>&nbsp;<div class="clearfix visible-xs-block"></div>
                    <strong>Email</strong> <?= Roots\Sage\Utils\hide_email(Roots\Sage\Option::get('email_address')); ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                <?php
                if (has_nav_menu('utility_navigation')):
                    wp_nav_menu(array('theme_location' => 'utility_navigation', 'menu_class' => 'list-inline list-unstyled'));
                endif;
                ?>

                <a href="http://corecreative.co.uk/" target="_blank" class="corecreative"><i class="icon icon-corecreative"></i></a>

            </div>
        </div>
    </div>
</footer>