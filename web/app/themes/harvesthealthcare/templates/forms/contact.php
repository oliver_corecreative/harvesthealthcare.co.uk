<form class="row cc_form" action="<?= CC_Form::get_vars()->url; ?>" method="post">
	<?= CC_Form::get_vars()->fields; ?>

	<div class="container-fluid">

		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="name" type="text" class="input-block form-control" placeholder="Name..." value="<?= @$_REQUEST['name']; ?>" />
				<sup class="required">*</sup>
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="company" type="text" class="input-block form-control" placeholder="Company/NHS Trust..." value="<?= @$_REQUEST['company']; ?>" />
				<sup class="required">*</sup>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="telephone" type="tel" class="input-block form-control" placeholder="Telephone..." value="<?= @$_REQUEST['telephone']; ?>" />
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="job-title" type="text" class="input-block form-control" placeholder="Job Title..." value="<?= @$_REQUEST['job-title']; ?>" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="address-line-1" type="text" class="input-block form-control" placeholder="Address Line 1..." value="<?= @$_REQUEST['address-line-1']; ?>" />
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="address-line-2" type="text" class="input-block form-control" placeholder="Address Line 2..." value="<?= @$_REQUEST['address-line-2']; ?>" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="town-city" type="text" class="input-block form-control" placeholder="Town/City..." value="<?= @$_REQUEST['town-city']; ?>" />
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="postcode" type="text" class="input-block form-control" placeholder="Postcode..." value="<?= @$_REQUEST['postcode']; ?>" />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="email" type="email" class="input-block form-control" placeholder="Email..." value="<?= @$_REQUEST['email']; ?>" />
				<sup class="required">*</sup>
			</div>
			<div class="form-group col-xs-12 col-sm-6">

				<select name="subject" class="form-control fancy">
					<option value="0" disabled selected>Subject...</option>
					<?= Roots\Sage\Utils\arrayToSelect(array(
						'General Information',
						'Product Information',
						'Servicing',
						'Quotation',
						'Distributors',
						'Other'
					), @$_REQUEST['subject']); ?>
				</select>

			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12">
				<textarea name="message" placeholder="How can we help?" class="input-block form-control" rows="9"><?= @stripslashes($_REQUEST['message']); ?></textarea>
				<sup class="required">*</sup>
			</div>
		</div>
		<div class="row">

			<div class="form-group col-xs-12 col-md-6">
				<label>
					<input type="checkbox" name="call-back" class="form-control fancy form-control-large" value="Yes" /> Request a call back
				</label>
			</div>
			<div class="form-group col-xs-12 col-md-6">
				<button type="submit" onClick="ga('send', 'event', 'Form', 'Submission', 'Contact Form');" class="btn btn-primary btn-block text-uppercase">Submit your enquiry</button>
			</div>

		</div>

	</div>

</form>