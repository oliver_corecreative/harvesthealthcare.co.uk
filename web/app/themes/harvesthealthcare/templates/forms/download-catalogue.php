<form class="row cc_form" action="<?= CC_Form::get_vars()->url; ?>" method="post">
	<?= CC_Form::get_vars()->fields; ?>

	<div class="container">
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="name" type="text" class="input-block form-control" placeholder="Name..." />
				<sup class="required">*</sup>
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="company" type="text" class="input-block form-control" placeholder="Company/NHS Trust..." />
				<sup class="required">*</sup>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="telephone" type="tel" class="input-block form-control" placeholder="Telephone..." />
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="job-title" type="text" class="input-block form-control" placeholder="Job Title..." />
				<sup class="required">*</sup>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="address-line-1" type="text" class="input-block form-control" placeholder="Address Line 1..." />
				<sup class="required">*</sup>
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="address-line-2" type="text" class="input-block form-control" placeholder="Address Line 2..." />
			</div>
		</div>
		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="town-city" type="text" class="input-block form-control" placeholder="Town/City..." />
				<sup class="required">*</sup>
			</div>
			<div class="form-group col-xs-12 col-sm-6">
				<input name="postcode" type="text" class="input-block form-control" placeholder="Postcode..." />
				<sup class="required">*</sup>
			</div>
		</div>

		<div class="row">
			<div class="form-group col-xs-12 col-sm-6">
				<input name="email" type="email" class="input-block form-control" placeholder="Email..." />
				<sup class="required">*</sup>
			</div>

			<div class="form-group col-xs-12 col-sm-6">

				<select name="file" class="form-control fancy">
					<option value="0" disabled selected>Choose Catalogue...</option>
					<?= arrayToSelect(array(
						'2015 Catalogue' => 'Main Catalogue 2015 Edition'
					)); ?>
				</select>

			</div>
		</div>

		<div class="row">
			<div class="col-xs-12 text-center">
				<button type="submit" onClick="ga('send', 'event', 'Form', 'Submission', 'Request Brochure');" class="btn btn-wide btn-primary text-uppercase">Submit Request</button>
			</div>
		</div>
	</div>

</form>