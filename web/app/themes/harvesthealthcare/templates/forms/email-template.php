<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		a {
			border: 0;
		}
		table, body {
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			font-size: 17px;
			color: #58585a;
			border: 0;
		}
		div {
			font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
			font-size: 17px;
			line-height: 22px;
		}
		div.heading {
			font-size: 20px;
			font-weight: bold;
			color: white;
		}
		div.spacer {
			height: 20px;
			line-height: 20px;
			font-size: 20px;
		}
	</style>
</head>
<body bgcolor="#EAEAEA" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>

		<td align="center" valign="center">

			<table width="600" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
				<tr>

					<td align="left" valign="middle" style="padding: 30px;" width="110" bgcolor="#506a81">

						<img src="<?= get_template_directory_uri() . '/dist/images/logo-white.png'; ?>" width="110" height="73" />


					</td>
					<td align="left" valign="middle" style="padding: 30px;" bgcolor="#506a81">
						<div class="heading">Website Enquiry : <?= ucwords($data['form']); ?></div>
					</td>

				</tr>
				<tr>

					<td align="left" valign="middle" style="padding: 30px;" colspan="2">

						<table width="540" cellpadding="5" cellspacing="0" border="1">
							<?php foreach($data['fields'] as $field): ?>
							<tr>
								<th align="right">
									<div><?= $field['label']; ?></div>
								</th>
								<td align="left">
									<div><?= nl2br($field['value']); ?></div>
								</td>
							</tr>
							<?php endforeach; ?>
						</table>
						<div class="spacer">&nbsp;</div>
						<div style="font-size: 12px;">Enquiry submitted on <?= $data['date']; ?> with an IP address of <?= $data['ip_address']; ?></div>

					</td>

				</tr>
			</table>

		</td>

	</tr>
</table>

</body>
</html>