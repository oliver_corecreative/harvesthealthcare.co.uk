<form class="row cc_form" action="<?= CC_Form::get_vars()->url; ?>" method="post">
	<?= CC_Form::get_vars()->fields; ?>

	<div class="form-group col-xs-12 col-sm-6">
		<input name="name" type="text" class="form-control input-block" placeholder="Name..." value="<?= @$_REQUEST['name']; ?>" />
		<sup class="required">*</sup>
	</div>
	<div class="form-group col-xs-12 col-sm-6">
		<input name="company" type="text" class="form-control input-block" placeholder="Company..." value="<?= @$_REQUEST['company']; ?>" />
	</div>
	<div class="form-group col-xs-12 col-sm-6">
		<input name="telephone" type="tel" class="form-control input-block" placeholder="Telephone..." value="<?= @$_REQUEST['telephone']; ?>" />
	</div>
	<div class="form-group col-xs-12 col-sm-6">
		<input name="email" type="email" class="form-control input-block" placeholder="Email..." value="<?= @$_REQUEST['email']; ?>" />
		<sup class="required">*</sup>
	</div>
	<div class="form-group col-xs-12">
		<textarea name="message" placeholder="Your enquiry..." class="form-control input-block" rows="4"><?= @stripslashes($_REQUEST['message']); ?></textarea>
	</div>

	<div class="form-group col-xs-12 col-md-4 col-md-offset-4">
		<button type="submit" onClick="ga('send', 'event', 'Form', 'Submission', 'Maintenance Request');" class="btn btn-primary btn-block text-uppercase pull-right">Submit your enquiry</button>
	</div>

</form>