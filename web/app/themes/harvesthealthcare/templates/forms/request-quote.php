<form class="row cc_form" action="<?= CC_Form::get_vars()->url; ?>" onsubmit="ga('send', 'event', 'Form', 'Submission', 'Quote Request');" method="post">
    <?= CC_Form::get_vars()->fields; ?>

    <div class="form-group col-xs-12 col-sm-6">
        <input name="name" type="text" class="form-control input-block" placeholder="Name..." />
        <sup class="required">*</sup>
    </div>
    <div class="form-group col-xs-12 col-sm-6">
        <input name="company" type="text" class="form-control input-block" placeholder="Company Name..." />
        <sup class="required">*</sup>
    </div>
    <div class="form-group col-xs-12">
        <input name="email" type="email" class="form-control input-block" placeholder="Email..." />
        <sup class="required">*</sup>
    </div>
    <div class="form-group col-xs-12 col-sm-6">
        <input name="telephone" type="text" class="form-control input-block" placeholder="Phone Number..." />
    </div>
    <div class="form-group col-xs-12 col-sm-6">
        <input name="quantity" type="number" class="form-control input-block" placeholder="Quantity..." />
        <sup class="required">*</sup>
    </div>
    <div class="form-group col-xs-12">
        <textarea name="message" placeholder="Your enquiry..." class="form-control input-block" rows="5"></textarea>
    </div>
    <input name="product" type="hidden" value="<?= $_REQUEST['params']['product']; ?>" />

</form>