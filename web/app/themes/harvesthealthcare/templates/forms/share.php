<form class="row cc_form" action="<?= CC_Form::get_vars()->url; ?>" method="post">
	<?= CC_Form::get_vars()->fields; ?>

	<div class="form-group col-xs-12 col-sm-6">
		<input name="name" type="text" class="input-block form-control" placeholder="Name..." />
		<sup class="required">*</sup>
	</div>
	<div class="form-group col-xs-12 col-sm-6">
		<input name="email" type="email" class="input-block form-control" placeholder="Email..." />
		<sup class="required">*</sup>
	</div>
	<div class="form-group col-xs-12 friend">
		<input name="friends_email[]" type="email" class="input-block form-control" placeholder="Friend's Email..." />
	</div>
	<div class="form-group col-xs-12">
		<button type="button" class="btn btn-primary form_minus">-</button>
		<button type="button" class="btn btn-primary form_plus">+</button>
		<label>Add or remove friends</label>
	</div>
	<div class="form-group col-xs-12">
		<textarea name="message" placeholder="Your message..." class="input-block form-control" rows="8"></textarea>
		<sup class="required">*</sup>
		<small>Your message will include a link to this product.</small>
	</div>
	<input name="product" type="hidden" value="<?= $_REQUEST['params']['product']; ?>" />
	<input name="url" type="hidden" value="<?= $_REQUEST['params']['url']; ?>" />

	<div class="col-xs-12 col-sm-8 checkbox-large">
		<label for="copy_to_own_email">
			<input type="checkbox" name="copy_to_own_email" value="1" id="copy_to_own_email" class="form-control form-control-large fancy" />
			Send a copy to my email address
		</label>
	</div>


</form>