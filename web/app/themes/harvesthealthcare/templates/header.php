<header class="header" role="banner">
    <div class="container">
        <div class="row top">
            <div class="col-xs-12 col-sm-6 col-md-5 logo-wrapper">
                <a class="logo-strapline hover-opacity" href="<?= Roots\Sage\Option::base(); ?>"><?= Roots\Sage\Option::wp('name'); ?></a>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 text-right block">
                <p>Call our sales team now<br /> on <a href="tel:<?= Roots\Sage\Option::get('phone_number'); ?>"><?= Roots\Sage\Option::get('phone_number'); ?></a> or email<br /> <?= Roots\Sage\Utils\hide_email(Roots\Sage\Option::get('email_address')); ?></p>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="text-right"><?php get_template_part("templates/blocks/social-icons"); ?></div>
                <?php get_template_part("templates/blocks/enquiry-basket-item"); ?>
                <div class="search-form-wrapper"><?= \Roots\Sage\Utils\get_search_form(); ?></div>
            </div>
        </div>
    </div>


</header>
<?php
get_template_part('templates/blocks/banner');
?>