<div class="modal fade" id="quote_modal" tabindex="-1" role="dialog" aria-labelledby="Request a Quote" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Request a Quote</h4>
            </div>
            <div class="modal-body">

                <p>To request a quote for this item, please complete your details below.</p>
                <p><strong>Product: </strong><?= $_REQUEST['params']['product']; ?></p>
                <?= CC_Form::display_form('request-quote', array()); ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="jQuery('#quote_modal form').submit();">Submit</button>
            </div>
        </div>
    </div>
</div>