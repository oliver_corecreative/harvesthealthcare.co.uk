<div class="modal fade" id="share_modal" tabindex="-1" role="dialog" aria-labelledby="Share This Product" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h3 class="modal-title">Share This Product</h3>
			</div>
			<div class="modal-body">

				<div class="entry-content">
					<p>Want to let someone else know about this great product? Simply complete the details below...</p>
					<p><strong>Product: </strong><?= $_REQUEST['params']['product']; ?></p>
				</div>
				<?= CC_Form::display_form('share', array()); ?>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" onclick="$('#share_modal form').submit();">Submit</button>
			</div>
		</div>
	</div>
</div>