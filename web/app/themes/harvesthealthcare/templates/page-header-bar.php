<?php use Roots\Sage\Titles; ?>

<div class="page-header">
    <h1 class="page-title h4 bar bar-secondary"><?= Titles\title(); ?></h1>
</div>
