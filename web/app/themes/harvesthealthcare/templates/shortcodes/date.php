<?php

$params = shortcode_atts( array(
    'format' => 'd/m/Y'
), $atts );

echo date($params['format']);