<?php
use Roots\Sage\Option;

$params = shortcode_atts( array(
    'key' => ''
), $atts );

echo '<span class="option">';
echo Option::get($params['key']);
echo '</span>';