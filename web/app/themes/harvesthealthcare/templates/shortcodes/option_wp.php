<?php
use Roots\Sage\Option;

$params = shortcode_atts( array(
    'key' => ''
), $atts );

if($params['key'] == "admin_email") echo \Roots\Sage\Utils\hide_email(Option::wp($params['admin_email']));
else echo Option::wp($params['key']);