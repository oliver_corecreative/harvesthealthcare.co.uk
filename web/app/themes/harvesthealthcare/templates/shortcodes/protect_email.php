<?php

$params = shortcode_atts( array(
    'email' => ''
), $atts );

if(empty($params['email'])) $params['email'] = \Roots\Sage\Option::get("email_address");

echo Roots\Sage\Utils\hide_email($params['email']);