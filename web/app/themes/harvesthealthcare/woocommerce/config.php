<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 26/05/2015
 * Time: 10:10
 */

/*
 * WC Template Hooks
 */

// remove woocommerce breadcrumbs
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// meta bar remove defaults
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

// meta bar add correctly
add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 10 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 10 );

//add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 20 );
//add_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 20 );

if(isset(Woocommerce_Products_Per_Page::instance()->front_end)) {
    //add_action('woocommerce_before_shop_loop', array(Woocommerce_Products_Per_Page::instance()->front_end, 'products_per_page_dropdown'), 30);
    //add_action('woocommerce_after_shop_loop', array(Woocommerce_Products_Per_Page::instance()->front_end, 'products_per_page_dropdown'), 30);
}

add_action( 'woocommerce_before_shop_loop', 'woocommerce_pagination', 40 );
add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 40 );


// remove read more button from loop
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'cc_template_short_description', 10 );
add_action( 'woocommerce_after_shop_loop_item_invisible', 'cc_template_long_description', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'cc_template_button', 15 );


remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product', 'woocommerce_upsell_display', 15 );


remove_action( 'woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail' );
add_action( 'woocommerce_after_subcategory_title', 'woocommerce_subcategory_thumbnail' );


// remove read more button from loop
add_action( 'woocommerce_after_single_product_summary', function() {

    wc_get_template( 'single-product/quicklinks.php' );

}, 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );



remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

/*
 * END : WC Template Hooks
 */

add_filter('pre_get_posts', function ($query) {
    if ($query->is_search) {
        $query->set('post_type', 'product');
        $query->set('meta_key', '_visibility');
        $query->set('meta_value', 'visible');
        if(function_exists('Woocommerce_Products_Per_Page') && Woocommerce_Products_Per_Page()->front_end) {
            $query->set('posts_per_page', Woocommerce_Products_Per_Page()->front_end->loop_shop_per_page());
        }
    }
    return $query;
});
add_filter('body_class', function ($classes) {
    global $wp_query;
    if($wp_query->is_search) {
        $classes[] = 'woocommerce';
        $classes[] = 'archive';
    }
    return $classes;
});

add_filter('is_woocommerce', function($default) {
    global $wp_query;
    if($wp_query->is_search) {
        $default = true;
    }
    return $default;
});


add_action('woocommerce_before_shop_loop_item', 'cc_product_badges');

function cc_product_badges() {
    global $product;

    if(has_term('clearance', 'product_cat')) {
        echo '<img src="'.\Roots\Sage\Assets\asset_path('images/badges/clearance.svg').'" alt="Reduced to clear" class="icon-badge" />';
    } elseif(get_post_meta($product->id, 'new', true) == 1) {
        echo '<img src="'.\Roots\Sage\Assets\asset_path('images/badges/new-badge.svg').'" alt="New Product" class="icon-badge" />';
    } elseif (get_post_meta($product->id, 'final_stock', true) == 1) {
        echo '<img src="'.\Roots\Sage\Assets\asset_path('images/badges/final-stock-badge.svg').'" alt="Final Stock" class="icon-badge" />';
    }

}



function cc_template_short_description($args = array()) {
    wc_get_template( 'loop/short-description.php' , $args );
}
function cc_template_long_description($args = array()) {
    wc_get_template( 'loop/long-description.php' , $args );
}
function cc_template_button($args = array()) {
    wc_get_template( 'loop/button.php' , $args );
}

add_filter('woocommerce_product_subcategories_hide_empty', function() {
    return __return_true();
});
add_filter('ywraq_product_added_view_browse_list', function() {
    return 'Browse your enquiry basket';
});


add_filter('woocommerce_page_title', function($title) {

    if(is_product_category() && get_queried_object()->parent) {
        $category = get_term_by('id', get_queried_object()->parent, get_queried_object()->taxonomy);
        $title = $category->name . " - " . $title;
    }
    return $title;

});