<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

global $product;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>

<div itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>" id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="container entry entry-main">
		<div class="row">

			<div class="entry-summary col-xs-12 col-md-6 col-md-push-6">

				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked cc_template_single_main_category - 3
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					//do_action( 'woocommerce_single_product_summary' );
				?>
				<?php
				$terms = get_the_terms(get_the_ID(), 'product_cat');

				usort($terms, function($a,$b) {
					return $a->parent-$b->parent;
				});
				$deepest_item = end($terms);
				$shallowest_item = array_shift($terms);

				$short_title = get_field('short_title', $deepest_item);
				$deepest_item->short_title = empty($short_title) ? $deepest_item->name : $short_title;

				?>

				<h3 class="entry-terms parent"><?= $shallowest_item->short_title; ?></h3>
				<?php woocommerce_template_single_title(); ?>
				<?php $sku = $product->get_sku(); if(!empty($sku)): ?>
					<h4 class="entry-sku">Code <span><?= $sku; ?></span></h4>
				<?php endif; ?>

				<div class="entry-content"><?php the_content(); ?></div>

				<?php
				woocommerce_template_single_price();

				global $product;

				$args         = array(
					'class'         => 'btn btn-primary btn-lg add-request-quote-button ',
					'wpnonce'       => wp_create_nonce( 'add-request-quote-' . $product->id ),
					'product_id'    => $product->id,
					'label'         => apply_filters( 'ywraq_product_add_to_quote' , get_option('ywraq_show_btn_link_text') ),
					'label_browse'  => apply_filters( 'ywraq_product_added_view_browse_list' , __( 'Browse your enquiry basket', 'yith-woocommerce-request-a-quote' ) ),
					'template_part' => 'button',
					'rqa_url'       => YITH_Request_Quote()->get_raq_page_url(),
					'exists'        => ( $product->product_type == 'variable' ) ? false : YITH_Request_Quote()->exists( $product->id )
				);
				$args['args'] = $args;

				wc_get_template('add-to-quote.php', $args, YITH_YWRAQ_DIR, YITH_YWRAQ_DIR);
				?>

			</div><!-- .summary -->

			<?php
			/**
			 * woocommerce_before_single_product_summary hook
			 *
			 * @hooked woocommerce_show_product_sale_flash - 10
			 * @hooked woocommerce_show_product_images - 20
			 */

			// oops!
			do_action( 'woocommerce_before_single_product_summary' );
			?>

		</div>
	</div>

	<section class="grey-bg after_single_product_summary">
		<div class="container">
			<div class="row">
			<?php
				/**
				 * woocommerce_after_single_product_summary hook
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
			?>
			</div>
		</div>
	</section>

	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
