<?php
/**
 * Sidebar
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     1.6.4
 */
?>
<div class="sidebar wc-sidebar">

    <div class="widget widget-menu">
        <h3 class="widget-title h4 bar bar-secondary">Product Categories</h3>
        <?php $product_categories = Roots\Sage\App::get_main_categories([
            'children' => true
        ]); ?>
        <ul class="widget-list list-unstyled">
            <?php $i = 0; foreach ($product_categories as $item): ?>
                <li class="<?= $i == 0 ? "first" : ""; ?> parent-item <?= is_tax() && (get_queried_object()->term_id == $item->term_id || get_queried_object()->parent == $item->term_id) ? "current-item" : ""; ?>">
                    <a href="<?= $item->permalink; ?>" title="<?= $item->name; ?>"><?= $item->name; ?></a>
                    <?php if(!empty($item->children)): ?>
                    <ul class="dropdown">
                        <?php $c = 0; foreach($item->children as $child): ?>
                            <li class="<?= $c == 0 ? "first" : ""; ?> child-item <?= is_tax() && (get_queried_object()->term_id == $child->term_id || get_queried_object()->parent == $child->term_id) ? "current-item" : ""; ?>">
                                <a href="<?= $child->permalink; ?>" title="<?= $child->name; ?>"><?= $child->name; ?></a>
                            </li>
                        <?php $c++; endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </li>
            <?php $i++; endforeach; ?>
        </ul>
    </div>

</div>