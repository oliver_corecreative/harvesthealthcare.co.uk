<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 26/05/2015
 * Time: 14:08
 */
$terms = get_the_terms(get_the_ID(), 'product_cat');

usort($terms, function($a,$b) {
    return $a->parent-$b->parent;
});
$deepest_item = end($terms);
?>
<h4 class="entry-terms"><?= $deepest_item->name; ?></h4>
