
<?php
/**
 * Single product short description
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
global $post;
if ( ! $post->post_content ) {
    return;
}


?>
<div itemprop="description" class="description entry-content">
    <?php the_content(); ?>
</div>