
<?php
/**
 * Single product short description
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
global $post;
if ( ! $post->post_excerpt && ! $post->post_content ) {
    return;
}


?>
<div itemprop="description" class="description entry-content">
    <?php

    add_filter('excerpt_length', 'cc_description_length');
    add_filter('excerpt_more', 'cc_description_read_more');

    if ( empty($post->post_excerpt) ) {
        echo apply_filters( 'woocommerce_short_description', wp_trim_excerpt());
    } else {
        echo apply_filters('woocommerce_short_description', $post->post_excerpt);
    }

    remove_filter('excerpt_length', 'cc_description_length');
    remove_filter('excerpt_more', 'cc_description_read_more');
    ?>
</div>