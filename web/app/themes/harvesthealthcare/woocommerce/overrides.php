<?php
/**
 * Created by PhpStorm.
 * User: iMac9
 * Date: 16/07/15
 * Time: 15:32
 */

function woocommerce_get_product_thumbnail( $size = 'shop_catalog') {
    global $post, $product;

    if ( has_post_thumbnail() ) {

        list($src, $w, $h) = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
        $sizes = get_image_sizes($size);

        return '<img src="'.\Roots\Sage\App::image($src, $sizes['width'], $sizes['height']).'" title="'.$post->post_title.'" alt="'.$post->post_title.'" width="'.$sizes['width'].'" height="'.$sizes['height'].'" />';

    } elseif ( !$product->is_visible() ) {
        return false;
    } elseif ( wc_placeholder_img_src() ) {
        return wc_placeholder_img( $size );
    }
}

function cc_description_length() {
    return 13;
}
function cc_description_read_more() {
    return '&hellip;';
}

/**
 * Check if we will be showing products or not (and not subcats only)
 *
 * @subpackage	Loop
 * @return bool
 */
function woocommerce_products_will_display() {
    if ( is_shop() )
        return get_option( 'woocommerce_shop_page_display' ) != 'subcategories';

    if ( ! is_product_taxonomy() && !is_search() )
        return false;

    if ( is_search() || is_filtered() || is_paged() )
        return true;

    $term = get_queried_object();

    if ( is_product_category() ) {
        switch ( get_woocommerce_term_meta( $term->term_id, 'display_type', true ) ) {
            case 'subcategories' :
                // Nothing - we want to continue to see if there are products/subcats
                break;
            case 'products' :
            case 'both' :
                return true;
                break;
            default :
                // Default - no setting
                if ( get_option( 'woocommerce_category_archive_display' ) != 'subcategories' )
                    return true;
                break;
        }
    }

    // Begin subcategory logic
    global $wpdb;

    $parent_id             = empty( $term->term_id ) ? 0 : $term->term_id;
    $taxonomy              = empty( $term->taxonomy ) ? '' : $term->taxonomy;

    if ( ! $parent_id && ! $taxonomy ) {
        return true;
    }

    $transient_name = 'wc_products_will_display_' . $parent_id . WC_Cache_Helper::get_transient_version( 'product_query' );

    if ( false === ( $products_will_display = get_transient( $transient_name ) ) ) {
        $has_children = $wpdb->get_col( $wpdb->prepare( "SELECT term_id FROM {$wpdb->term_taxonomy} WHERE parent = %d AND taxonomy = %s", $parent_id, $taxonomy ) );

        if ( $has_children ) {
            // Check terms have products inside - parents first. If products are found inside, subcats will be shown instead of products so we can return false.
            if ( sizeof( get_objects_in_term( $has_children, $taxonomy ) ) > 0 ) {
                $products_will_display = false;
            } else {
                // If we get here, the parents were empty so we're forced to check children
                foreach ( $has_children as $term ) {
                    $children = get_term_children( $term, $taxonomy );

                    if ( sizeof( get_objects_in_term( $children, $taxonomy ) ) > 0 ) {
                        $products_will_display = false;
                        break;
                    }
                }
            }
        } else {
            $products_will_display = true;
        }
    }

    set_transient( $transient_name, $products_will_display, DAY_IN_SECONDS * 30 );

    return $products_will_display;
}