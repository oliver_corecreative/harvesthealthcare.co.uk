<?php
/**
 * Form to Request a quote
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @version 1.0.0
 * @author  Yithemes
 */
$current_user = array();
if ( is_user_logged_in() ) {
    $current_user = get_user_by( 'id', get_current_user_id() );
}

$user_name = ( ! empty( $current_user ) ) ?  $current_user->display_name : '';
$user_email = ( ! empty( $current_user ) ) ?  $current_user->user_email : '';
?>
<div class="yith-ywraq-mail-form-wrapper">
    <h3><?php _e( 'Your Details', 'yith-woocommerce-request-a-quote' ) ?></h3>

    <div class="grid-reduced">
        <div class="row">
            <div class="col-xs-12">
                <form id="yith-ywraq-mail-form" name="yith-ywraq-mail-form" action="<?php echo esc_url( YITH_Request_Quote()->get_raq_page_url() ) ?>" method="post" class="row">

                    <div class="col-xs-12 col-md-6 validate-required" id="rqa_name_row">
                        <div class="form-group">
                            <input type="text" class="input-block form-control" name="rqa_name" id="rqa-name" value="<?php echo $user_name ?>" required placeholder="Your Name*">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 validate-required" id="rqa_email_row">
                        <div class="form-group">
                            <input type="email" class="input-block form-control" name="rqa_email" id="rqa-rqa_email" value="<?php echo $user_email ?>" required placeholder="Email Address*">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6" id="rqa_company_row">
                        <div class="form-group">
                            <input type="text" class="input-block form-control" name="rqa_company" id="rqa-rqa_company" required placeholder="Company Name*">
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6 validate-required" id="rqa_telephone_row">
                        <div class="form-group">
                            <input type="tel" class="input-block form-control" name="rqa_telephone" id="rqa-rqa_telephone" placeholder="Phone Number">
                        </div>
                    </div>
                    <div class="col-xs-12 validate-required" id="rqa_message_row">
                        <div class="form-group">
                            <textarea name="rqa_message" class="input-block form-control" id="rqa-message" rows="8" placeholder="Additional Notes"></textarea>
                        </div>
                    </div>

                    <div class="col-xs-12 text-xs-center">
                        <button type="submit" onClick="ga('send', 'event', 'Form', 'Submission', 'Quote Request');" class="btn btn-primary btn-lg raq-send-request">Submit quote request</button>
                        <input type="hidden" id="raq-mail-wpnonce" name="raq_mail_wpnonce" value="<?php echo wp_create_nonce( 'send-request-quote' ) ?>">
                    </div>

                </form>
            </div>

        </div>
    </div>

</div>