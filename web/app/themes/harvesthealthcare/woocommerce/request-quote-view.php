<?php
/**
 * Table view to Request A Quote
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @author  Yithemes
 */

global $post;
if (count($raq_content) == 0):
    ?>
    <p>Your enquiry basket is empty.</p>
    <a href="/shop/" class="btn btn-primary">View our product range</a>
<?php else: ?>
    <form id="yith-ywraq-form" name="yith-ywraq-form" action="<?php echo esc_url(YITH_Request_Quote()->get_raq_page_url('update')) ?>?updated=1" method="post">
        <h3><?php _e('Your Selected Products', 'yith-woocommerce-request-a-quote') ?></h3>
        <table class="shop_table cart" id="yith-ywrq-table-list" cellspacing="0">
            <thead>
            <tr>
                <th class="product-remove">&nbsp;</th>
                <th class="product-name"><?php _e( 'Product', 'yith-woocommerce-request-a-quote' ) ?></th>
                <th class="product-thumbnail">&nbsp;</th>
                <th class="product-quantity"><?php _e( 'Quantity', 'yith-woocommerce-request-a-quote' ) ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($raq_content as $key => $raq):

                $raq_id = isset( $raq['variation_id'] ) ? $raq['variation_id'] : $raq['product_id'];
                $_product = wc_get_product( $raq_id );
                if( !isset( $_product ) || !is_object($_product) ) continue;

                $post = get_post($raq_id);
                setup_postdata($raq_id);
                ?>
                <tr class="cart_item">

                    <td class="product-remove" width="40">
                        <?php
                        echo apply_filters('yith_ywraq_item_remove_link', sprintf('<a href="#"  data-remove-item="%s" data-wp_nonce="%s"  data-product_id="%d" class="yith-ywraq-item-remove remove" title="%s">&times;</a>', $key, wp_create_nonce('remove-request-quote-' . $_product->id), $_product->id, __('Remove this item', 'yith-woocommerce-request-a-quote')), $key);
                        ?>
                        <img src="<?php echo esc_url(admin_url('images/wpspin_light.gif')) ?>" class="ajax-loading" alt="loading" width="16" height="16" style="visibility:hidden"/>
                    </td>

                    <td class="product-thumbnail" width="200">
                        <?= sprintf('<a href="%s" class="hover-opacity">%s</a>', $_product->get_permalink(), woocommerce_get_product_thumbnail('shop_catalog', 3)); ?>
                    </td>

                    <td class="product-name">
                        <a href="<?php echo $_product->get_permalink() ?>">
                            <h4 class="entry-title">
                                <strong><?= $_product->get_sku(); ?></strong> : <?php echo $_product->get_title() ?>
                                <?php if($_product->get_price()): ?>
                                <span class="price">(<?php echo $_product->get_price_html(); ?>)</span>
                                <?php endif; ?>
                            </h4>
                        </a>
                    </td>
                    <td class="product-quantity" width="200">
                        <?php
                        $product_quantity = woocommerce_quantity_input( array(
                            'input_name'  => "raq[{$key}][qty]",
                            'input_value' => $raq['quantity'],
                            'max_value'   => apply_filters('ywraq_quantity_max_value', $_product->backorders_allowed() ? '' : $_product->get_stock_quantity() ),
                            'min_value'   => '0'
                        ), $_product, false );

                        echo $product_quantity;
                        ?>
                    </td>
                </tr>

            <?php endforeach;
            wp_reset_postdata(); ?>

            <tr>
                <td colspan="4" class="actions">
                    <input type="submit" class="btn btn-default" name="update_raq" value="<?php _e('Update List', 'yith-woocommerce-request-a-quote') ?>">
                    <input type="hidden" id="update_raq_wpnonce" name="update_raq_wpnonce" value="<?php echo wp_create_nonce( 'update-request-quote-quantity' ) ?>">
                </td>
            </tr>

            </tbody>
        </table>
    </form>
<?php endif ?>

