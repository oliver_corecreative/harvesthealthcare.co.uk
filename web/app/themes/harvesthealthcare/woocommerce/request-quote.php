<?php
/**
 * Request A Quote pages template; load template parts
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @author  Yithemes
 */

global $wpdb, $woocommerce;

if( function_exists( 'wc_print_notices' ) ) {
    yith_ywraq_print_notices();
}

?>
<div class="woocommerce ywraq-wrapper row">
	<div id="yith-ywraq-message col-xs-12"></div>

    <?php if(isset($_GET['updated']) && $_GET['updated'] == 1): ?>
    <div class="col-xs-12">
        <p class="woocommerce-message">Your enquiry basket has been updated.</p>
    </div>
    <?php endif; ?>

    <div class="col-xs-12">
        <?php wc_get_template( 'request-quote-' . $template_part . '.php', $args, YITH_YWRAQ_DIR, YITH_YWRAQ_DIR );  ?>
    </div>

    <?php if( count($raq_content) != 0): ?>

        <div class="col-xs-12">
            <?php wc_get_template( 'request-quote-form.php', $args, YITH_YWRAQ_DIR, YITH_YWRAQ_DIR );  ?>
        </div>

    <?php endif ?>
</div>