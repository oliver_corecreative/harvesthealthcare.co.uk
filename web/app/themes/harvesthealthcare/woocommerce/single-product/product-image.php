<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.14
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $woocommerce, $product;

?>
<div class="col-xs-12 col-md-6 col-md-pull-6">
	<div class="images">

		<?php
			if ( has_post_thumbnail() ) {

				$image_title 	= esc_attr( get_the_title( get_post_thumbnail_id() ) );
				$image_caption 	= get_post( get_post_thumbnail_id() )->post_excerpt;
				$image_link  	= wp_get_attachment_url( get_post_thumbnail_id() );
				$image       	= wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID), apply_filters( 'single_product_large_thumbnail_size', 'large' ));

				$image = '<img src="'.\Roots\Sage\App::image($image[0], 600, 400).'" class="attachment-large wp-post-image" alt="'.$image_title.'" title="'.$image_title.'">';

				$attachment_count = count( $product->get_gallery_attachment_ids() );

				if ( $attachment_count > 0 ) {
					$gallery = 'product-gallery';
				} else {
					$gallery = '" data-rel="prettyPhoto';
				}

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image ' . $gallery . '" title="%s" >%s</a>', $image_link, $image_caption, $image ), $post->ID );

			} else {

				echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );

			}
		?>

	</div>

	<div class="product-thumbnails">
		<?php do_action( 'woocommerce_product_thumbnails' ); ?>
	</div>

</div>
