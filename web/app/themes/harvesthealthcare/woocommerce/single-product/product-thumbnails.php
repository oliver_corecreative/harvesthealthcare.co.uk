<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	$loop 		= 0;
	//$columns 	= apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
	?>
	<a href="#" class="pager prev">
		<i class="icon icon-arrow-left"></i>
	</a>
	<div class="slider-wrapper">
		<div class="row"><?php

			foreach ( $attachment_ids as $attachment_id ) {

				$classes = array( 'zoom' );

				$image_link = wp_get_attachment_url( $attachment_id );

				$classes[] = 'img-responsive';

				if ( ! $image_link )
					continue;

				$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
				$image_class = esc_attr( implode( ' ', $classes ) );
				$image_title = esc_attr( get_the_title( $attachment_id ) );

				echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<div class="col-xs-3"><a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a></div>', $image_link, $image_class, $image_title, $image ), $attachment_id, $post->ID, $image_class );

				if($loop % 4 == 3 && count($attachment_ids) < $loop) {
					echo '</div><div class="row">';
				}

				$loop++;

			}

		?></div>
	</div>
	<a href="#" class="pager next">
		<i class="icon icon-arrow-right"></i>
	</a>
	<?php
}
