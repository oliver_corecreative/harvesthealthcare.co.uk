<?php
global $post;
?>
<div class="col-xs-12">
    <section class="quicklinks">

        <a href="/contact-us/?message=<?= urlencode('I am interested in "' . $post->post_title . '". Please send me more information.'); ?>&subject=<?= urlencode('Product Information'); ?>" class="text-left">
            <i class="icon icon-email"></i>
            Enquire about this product
        </a>
        <a href="#" data-modal="share" data-modal-params='<?= htmlspecialchars(json_encode(array( 'url' => get_permalink(), 'product' => $post->post_title ))); ?>' class="text-right">
            <i class="icon icon-envelope"></i>
            Share this product
        </a>

    </section>
</div>