<div class="row">
    <?php foreach($tab['content'] as $item): ?>

        <div class="col-xs-12 col-sm-6">
            <div class="media">
                <div class="media-left">
                    <img class="media-object" width="80" src="<?= $item['icon']['url']; ?>" alt="<?= $item['icon']['title']; ?>" />
                </div>
                <div class="media-body">
                    <p><?= $item['text']; ?></p>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</div>