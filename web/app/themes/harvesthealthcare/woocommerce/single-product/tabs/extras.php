<div class="row">
    <?php foreach($tab['content'] as $item): ?>

        <div class="col-xs-12 col-md-6">
            <div class="media-wrapper">
                <div class="media">
                    <div class="media-left">
                        <img class="media-object" width="180" src="<?= $item['image']['url']; ?>" alt="<?= $item['image']['title']; ?>" />
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><?= $item['title']; ?></h4>
                        <div class="entry-content"><?= $item['content']; ?></div>
                    </div>
                </div>
            </div>
        </div>

    <?php endforeach; ?>
</div>