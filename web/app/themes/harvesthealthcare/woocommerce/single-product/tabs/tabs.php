<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */


$tabs = array();

$features = get_field('features');
$prr = get_field('prr');
if(!empty($features) || !empty($prr)) {
	$tabs['features'] = array(
		'title' => 'Features',
		'content' => $features
	);
}

$additional_features = get_field('additional_features');
if(!empty($additional_features)) {
	$tabs['additional_features'] = array(
		'title' => 'Additional Features',
		'content' => $additional_features
	);
}

$specifications = get_field('specifications');
if(!empty($specifications)) {
	$tabs['specifications'] = array(
		'title' => 'Specifications',
		'content' => $specifications
	);
}

$extras = get_field('extras');
if(!empty($extras)) {
	$tabs['extras'] = array(
		'title' => 'Extras',
		'content' => $extras
	);
}

//$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs col-xs-12">
		<ul class="tabs">
			<?php foreach ( $tabs as $key => $tab ) : ?>

				<li class="<?= $key; ?>_tab" style="width: <?= floor(100/count($tabs)); ?>%;">
					<a href="#tab-<?= $key; ?>"><?= $tab['title']; ?></a>
				</li>

			<?php endforeach; ?>
		</ul>
		<?php foreach ( $tabs as $key => $tab ) : ?>

			<div class="panel tab entry-content tab-<?= $key; ?>" id="tab-<?= $key; ?>">
				<?php $template = wc_locate_template('single-product/tabs/' . $key . '.php'); ?>
				<?php if(file_exists($template)): ?>
					<?php require $template; ?>
				<?php else: ?>
					<?= $tab['content']; ?>
				<?php endif; ?>
			</div>

		<?php endforeach; ?>
	</div>

<?php endif; ?>
